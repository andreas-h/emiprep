===============
Getting started
===============

As emiprep is still in development, this document tries to give an
introduction to using emiprep.


emiprep configuration
---------------------

emiprep reads all runtime parameters from a configuration file in
YAML_ format.  An example file is available in the code tree at
``emiprep/config/tno_crimech.yaml``.

.. _YAML: https://yaml.org/


emiprep input files
-------------------

emiprep needs a lot of input data in order to run:

1. The emission inventories to be used themselves
2. Data about temporal (i.e., diurnal, weekly, seasonal) cycles.
   Currently, using data from EMEP is the only option (see Sect. 6.1.2
   in :cite:`simpson-2012-emep_msc`).
3. Data about the vertical distribution of emissions.  Currently,
   using data from EMEP is the only option (see Sect. 6.1.1 and
   Tab. S3 in :cite:`simpson-2012-emep_msc`).
4. Information about how to split emission species into model species
5. Information about the model domain.  This is currently read from
   ``wrfinput`` and ``met_em`` files.


Running emiprep
---------------

After activating the conda environment (see :doc:`development` for
details), emiprep can be executed (from the repository's root
directory) with the command

.. code:: shell
	  
   $ python -m emiprep.run.runner -c emiprep/config/tno_crimech.yaml


More information
----------------

More information can currently be found in some Jupyter Notebooks in
the :doc:`background/index` section.


References
----------

.. bibliography:: references.bib
