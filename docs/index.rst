.. emiprep documentation master file, created by
   sphinx-quickstart on Tue Aug 22 09:56:35 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=======
emiprep
=======

-------------------------------------------------------
Emission pre-processor for atmospheric chemistry models
-------------------------------------------------------

emiprep is yet another emission pre-processor for use with atmospheric
chemistry models.  It is being developed for `WRF-Chem
<https://ruc.noaa.gov/wrf/wrf-chem/>`__, but there is no reason why it
shouldn't in principle also work for other models.


.. warning::
   
   emiprep is currently in *pre-alpha* stage.  It is currently not
   usable yet, even though this documentation might read as if it
   were.  For the time being, please refer to the
   :doc:`gettingstarted` documentation.


About emiprep
=============

Preparing emission data is one of the crucial points in setting up a
study involving atmospheric chemistry models.  Therefore, many scripts
already exist for performing (parts of) this process.  However, these
tools are often very much tailor-made for a specific emission
inventory, and always geared towards a single model.  Hence, they are
often not very flexible, and it is hard to extend them.

At the same time, there is a common set of operations which each of these
scripts has to perform, e.g., re-gridding from emissions to model grid, applying
temporal (seasonal, weekly, diurnal) scaling factors, speciation of emission
species to model species.  Implementing these procedures is a significant source
of errors, and the modeling community as a whole would benefit from a common
implementation.

emiprep aims to be an *expandable* and *flexible* software package for emission
preparation.  The goal is to have one system which can easily be extended to
accommodate new emission inventories, or to modify temporal cycles of emissions.

Also, empirep is fully *open-source*, its development being organized in a
`GitLab project <https://gitlab.com/andreas-h/emiprep>`__.  This means every
interested person can download and modify the source code, and contributions
from to the emiprep source code are simple.

We would very much like emiprep to be a *community effort*, so we
genuinely appreciate any feedback, comments, and criticism.  
emiprep is written in the *Python* programming language.  Python is
fully open-source and has gained large traction in the scientific
community during the past years.


Feedback / Bug reports
======================

Feedback can be given either directly on the GitLab platform or via `e-mail
<incoming+andreas-h/emiprep@gitlab.com>`__ (please be aware that all e-mails to
this address will be public on the GitLab platform).


Mailing list
============

There is a mailing list `emiprep-discuss
<https://groups.io/g/emiprep-discuss>`__ on the Groups.io platform to serve as
community forum for all discussion regarding how to use emiprep.  To subscribe
to the mailing list, please send an e-mail to
emiprep-discuss+subscribe@groups.io.


Contents
========

.. toctree::
   :maxdepth: 2

   Installation <install>
   Getting started <gettingstarted>
   Development <development>
   Acknowledgements <acknowledgements>
   api
   Architecture <architecture/index>
   background/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
