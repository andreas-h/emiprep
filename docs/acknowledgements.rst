*********************
Acknowledgements
*********************

External data sources
=====================

Temporal cycles
---------------

The temporal cycle data files contained in the directory
``tests/testdata/emep`` have been provided by Agnes Nyiri (see `here
<https://gitlab.com/andreas-h/emiprep/issues/46#note_41108583>`__).
In the article :cite:`simpson-2012-emep_msc`, the temporal
distribution is described and the temporal factors (which are derived
largely from data collected by the University of Stuttgart (IER) as
part of the `GENEMIS project
<http://genemis.ier.uni-stuttgart.de/>`__) are properly referred.


References
==========

.. bibliography:: references.bib
