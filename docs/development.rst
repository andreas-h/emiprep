======================
Development of emiprep
======================

Repository
==========

The emiprep repository lives at `GitLab
<https://gitlab.com/andreas-h/emiprep>`__.  Source code is managed using `Git
<https://git-scm.com/>`__ version control.  emiprep adopts the `branching model
by Vincent Driessen
<http://nvie.com/posts/a-successful-git-branching-model/>`__.


Setting up the development environment
======================================

The simplest way to set up a development environment is via conda_:

.. code:: shell

   $ git clone git@gitlab.com:YOURUSERNAME/emiprep.git
   $ cd emiprep
   $ conda create -n emiprep_dev --file requirements_dev.txt

Now, whenever you want to work on *emiprep*, you simply have to activate this
environment:

.. code:: shell

   $ conda activate emiprep_dev

.. _conda: https://docs.continuum.io/docs_oss/conda/


Packaging
=========

emiprep has been uploaded to `PyPI <https://pypi.python.org/pypi/emiprep/>`__.


Links
-----

For setting up the repository, I have followed the following guides:

- https://python-packaging.readthedocs.io/en/latest/minimal.html
- https://docs.pytest.org/en/latest/goodpractices.html#integrating-with-setuptools-python-setup-py-test-pytest-runner
- https://packaging.python.org/tutorials/distributing-packages/


Version numbers
===============

emiprep uses versioneer_ for managing version numbers.

.. _versioneer: https://github.com/warner/python-versioneer


Testing
=======

emiprep uses `pytest <https://docs.pytest.org/>`__ for testing.


Python 2/3 compatibility
========================

The compatibility between Python 2/3 follows
http://python-future.org/overview.html.  As a guideline, each source file should
start with the following ``__future__`` imports::

   from __future__ import (
       absolute_import, division, print_function, unicode_literals)

Also, the following builtins should be imported from the ``builtins`` module
(provided by the `future <https://pypi.python.org/pypi/future/0.16.0>`__ module)
whenever used::

   from builtins import (ascii, bytes, chr, dict, filter, hex, input,
                         int, map, next, oct, open, pow, range, round,
                         str, super, zip)

Code written that way is *supposed* to behave identically on Python 2/3, but
apparently that's not always the case (see `#50
<https://gitlab.com/andreas-h/emiprep/issues/50>`__).


Making a release
================

1. Branch ``release-x.y.z`` off ``develop``:

   .. code:: shell

      $ git checkout develop
      $ git checkout -b release-x.y.z

2. Make last changes to ``release-x.y.z``.  At least, you should replace the
   *Unreleased* heading in ``docs/changelog.rst`` with the release version and
   date.

3. Merge the ``release-x.y.z`` branch into ``master``:

   .. code:: shell

      $ git checkout master
      $ git merge --no-ff release-x.y.z

4. Create a tag for the release:

   .. code:: shell

      $ git tag -a vx.y.z

   You will be asked to enter a tag annotation.          

   .. note::

      The automatic build and upload of conda packages is triggered by any new
      tag in the repository.  Whenever a new tag is pushed to the main GitLab
      repository, the CI build will get the most recent release from PyPI, build
      this, and upload to anaconda.org.  Therefore it is important to only push
      the new tag to gitlab *after* uploading the build to PyPI.

5. Create the source distribution:

   .. code:: shell

      $ python setup.py sdist

6. Upload this source distribution to PyPI:

   .. code:: shell

      $ twine upload --repository pypi dist/emiprep-x-y-z.tar.gz

7. Push the release to GitLab:

   .. code:: shell

      $ git push origin master
      $ git push --tags

8. Merge the "last changes" (see point 2. above) back into ``develop``:

   .. code:: shell

      $ git checkout develop
      $ git merge --no-ff release-x.y.z
