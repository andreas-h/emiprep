*******************************
Notes on *emiprep* architecture
*******************************

This section contains notes on the architecture of *emiprep*.

The general *emiprep* architecture is best captured by this :download:`mind map
<mindmap.xmind>` (you'll need the XMind_ application to edit the file):

.. _XMind: https://xmind.net/

.. warning:: This mind map is out-of-date!

   The actual flow should be something like

   1. For each emission dataset:

      1. read data
      2. geo-reference emission source
      3. vertical distribution
      4. unit conversion
      5. species split
      6. geographical interpolation to model grid

   2. For each time step:

      1. For each emission dataset:

         1. Apply time factors

      2. Merge inventories / Aggregate sectors
      3. Write output file

.. thumbnail:: mindmap.png
   :alt: emiprep architecture mind map

Following are more detailed discussions about specific choices made in the
emiprep architecture:

.. toctree::
   :maxdepth: 2

   wrfgrid
   regrid
