import glob
import logging
import os.path
# import warnings

import dask.dataframe as dd
import pandas as pd
import pycountry
import pytz

from ..config import cfg

NPROC = 24  # only used for joblib country hour index


SPECIES_MAP_EMEP_TO_TNO = pd.DataFrame(
    {'species_emep': [
        'CO', 'NH3', 'NOX', 'PM25', 'PMCO', 'SOX', 'VOC', 'VOC'],
     'species_tno': [
         'CO', 'NH3', 'NOX', 'PM_FINE', 'PM_COARSE', 'SO2', 'NMVOC', 'CH4']})


# HOURLY FACTORS
# ==============

def _get_timezone_single(ccode):
    try:
        country = pycountry.countries.get(alpha_3=ccode)
        # get 2-letter code for each country
        country2 = country.alpha_2
        # get timezone strings for each 2-letter code
        # TODO better selection necessary (don't always take first instance)
        tzname = pytz.country_timezones[country2][0]
        return tzname
    except (KeyError, AttributeError):
        if ccode == 'SCG':
            return 'Europe/Belgrade'
        else:
            return 'utc'


def add_timezone(emis, nproc=None):
    tz = emis['country'].drop_duplicates().to_frame().compute().set_index(
        'country')
    timezones = {c_: _get_timezone_single(c_) for c_ in tz.index.values}
    tz['tz'] = pd.Series(timezones)
    emis = emis.merge(tz, left_on='country', right_index=True)
    return emis


def add_hour_index_emep(emis, timestamp):
    """Get hour index of hourly emission factor for EMEP data

    Parameters
    ----------
    emis : dd.DataFrame

    timestamp : pd.Timestamp

    """
    if isinstance(emis, dd.DataFrame):
        emis_is_dask = True
        emis = emis.compute()
    else:
        emis_is_dask = False

    # add requested timestamp to emission DataFrame
    emis['timestamp'] = timestamp

    # from https://stackoverflow.com/a/47822370/152439
    ltime = emis.groupby('tz')['timestamp'].transform(
        lambda x: x.dt.tz_localize(x.name))

    del emis['timestamp']

    utcoffset = timestamp - ltime
    hix = timestamp.hour + utcoffset.dt.total_seconds() / 3600
    emis['ix_hour'] = hix.astype(int)

    if emis_is_dask:
        emis = dd.from_pandas(emis, chunksize=cfg.runtime.dask_bs)
    return emis


def load_emep_hourly_factors(path):
    """Load EMEP monthly factors from original MonthFac.XXX files"""
    df = pd.read_csv(
        path, comment='#', delim_whitespace=True,
        names=['weekday', 'sect_emep'] + list(range(24)))
    return df


def prepare_emep_hourly_factors(path, timestamp, normalize):
    """Prepare EMEP hourly factors for use"""

    df = load_emep_hourly_factors(path)
    df = select_emep_hourly_factors(df, timestamp, normalize)
    df = convert_sectors_emep_to_tno(df)

    return df


def select_emep_hourly_factors(df, timestamp, normalize=False):
    """Select only the requested hour from hourly factor DataFrame"""
    if normalize:
        weights = df.copy()
        weights = weights.sum(axis=1).values / 24.0
    df['weight_daily'] = df[timestamp.hour]
    for hour in range(24):
        df.drop(hour, axis=1, inplace=True)

    if normalize:
        df['weight_daily'] /= weights

    # select correct weekday
    df = df[df['weekday'] == timestamp.isoweekday()]
    df = df.drop(columns=['weekday'])

    return df


def add_emep_hourly_factors(emis, h_factors, timestamp):
    # TODO make distinction between different sources of hourly factors

    # EMEP diurnal cycle doesn't depend on species and country, only on sector
    h_factors = h_factors.sort_values(by='sect')

    # for each emission source find which local hour corresponds to a given
    # UTC hour
    emis = add_hour_index_emep(emis, timestamp)

    if 'weekday' not in emis.columns:
        emis['weekday'] = timestamp.isoweekday()

    fact_hour = emis.merge(h_factors, on=['sect'])

    check_time_factor_apply(emis, fact_hour, 'hourly')

    return fact_hour


# MONTHLY FACTORS
# ==============

def load_emep_monthly_factors(path):
    """Load EMEP monthly factors from original MonthFac.XXX files"""
    df = pd.read_csv(
        path, comment='#', delim_whitespace=True,
        names=['country_emep', 'sect_emep'] + list(range(1, 13)))
    return df


def convert_countries_emep_to_tno(df, country_map, extra_countries=None):
    """Add country ISO codes to DataFrame"""
    df = df.merge(
        country_map, left_on='country_emep', right_on='emep_country_id')
    df.drop(['emep_country_name', 'emep_country_id'], axis=1, inplace=True)
    df.rename(columns=dict(emep_iso='country'), inplace=True)

    if extra_countries is None:
        extra_countries = []
    elif isinstance(extra_countries, str):
        extra_countries = [extra_countries]

    df_new = []
    c0 = df['country_emep'].unique()[0]
    for cn in extra_countries:
        tmpsel_ = df[df['country_emep'] == c0].copy()
        mapping_row = country_map[country_map['emep_iso2'] == cn].iloc[0]
        for key in tmpsel_.columns:
            if key == 'country_emep':
                tmpsel_[key] = mapping_row['emep_country_id']
            elif key.startswith('weight_'):
                tmpsel_[key] = 1.0
            if key.startswith('emep_iso'):
                tmpsel_[key] = cn
            else:
                pass
        df_new.append(tmpsel_)

    if df_new:
        return pd.concat([df] + df_new)
    else:
        return df


def convert_sectors_emep_to_tno(df):
    """Add sector codes to DataFrame"""
    # add new column with tno sectors
    df['sect'] = df['sect_emep'].copy()

    # add duplicate sectors
    SECTOR_DUPLICATE = [
        (3, 34),
        (7, 71),
        (7, 72),
        (7, 73),
        (7, 74),
        (7, 75),
    ]

    df_new = []
    for k, v in SECTOR_DUPLICATE:
        tmpsel_ = df[df['sect_emep'] == k].copy()
        tmpsel_['sect'] = v
        df_new.append(tmpsel_)

    # add those sectors to the temporal factors which are supposed to
    # have no temporal variation.  This is needed because sector 81,
    # e.g., isn't represented in the EMEP dataset, but it is present
    # in TNO emissions.  If we don't account for that here, adding
    # weights of 1.0, the whole sector whould not be contained in the
    # output.
    SECTOR_UNIFORM = [81]
    for v in SECTOR_UNIFORM:
        tmpsel_ = df[df['sect_emep'] == k].copy()
        for key in tmpsel_:
            if key == 'sect':
                tmpsel_[key] = v
            elif key == 'sect_emep':
                tmpsel_[key] = None
            elif key.startswith('weight_'):
                tmpsel_[key] = 1.0
        df_new.append(tmpsel_)

    df = pd.concat([df] + df_new)

    return df


def select_emep_monthly_factors(df, timestamp, normalize=False):
    """Select only the requested month from monthly factor DataFrame"""
    if normalize:
        weights = df.copy()
        for col in ['country_emep', 'sect_emep']:
            weights.drop(col, axis=1, inplace=True)
        weights = weights.sum(axis=1).values / 12.0
    df['weight_annual'] = df[timestamp.month]
    for mon in range(1, 13):
        df.drop(mon, axis=1, inplace=True)

    if normalize:
        df['weight_annual'] /= weights

    return df


def prepare_emep_monthly_factors(path, country_map, timestamp, normalize,
                                 extra_countries=None):
    """Prepare EMEP monthly factors for use"""
    # which files are there?
    fns = glob.glob(os.path.join(path, 'MonthlyFac.*'))

    # which species are in these files?
    species = [os.path.splitext(f_)[1][1:].upper() for f_ in fns]

    m_factors = []
    for sp_, fn_ in zip(species, fns):
        df = load_emep_monthly_factors(fn_)
        df = select_emep_monthly_factors(df, timestamp, normalize)
        df = convert_countries_emep_to_tno(df, country_map, extra_countries)
        df = convert_sectors_emep_to_tno(df)
        df['species'] = sp_
        m_factors.append(df)

    m_factors = pd.concat(m_factors)

    return m_factors


def add_emep_monthly_factors(emis, m_factors, timestamp):
    # TODO make distinction between different sources of monthjly factors

    # species mapping for h_factors
    t_factors = m_factors.merge(
        SPECIES_MAP_EMEP_TO_TNO, left_on='species', right_on='species_emep')
    del t_factors['species']
    t_factors = t_factors.rename(columns={'species_tno': 'species'})
    t_factors = t_factors.sort_values(by=['sect', 'species', 'emep_iso3'])

    # for each emission source find which local hour corresponds to a given
    # UTC hour
    emis['ix_month'] = timestamp.month

    # TODO implement species mismatch warning
    # species_data = set(emis['species'].unique())
    # species_fact = set(t_factors['species'].unique())
    # if species_data != species_fact:
    #     warnings.warn(
    #         "Emission dataset includes the species {}, while monthly "
    #         "factors have {}".format(species_data, species_fact))

    fact_month = emis.merge(
        t_factors,
        left_on=['sect', 'species', 'country'],
        right_on=['sect', 'species', 'emep_iso3'])

    check_time_factor_apply(emis, fact_month, 'monthly')

    return fact_month


# DAILY FACTORS
# ==============

def load_emep_daily_factors(path):
    """Load EMEP daily factors from original DailyFac.XXX files"""
    df = pd.read_csv(
        path, comment='#', delim_whitespace=True,
        names=['country_emep', 'sect_emep'] + list(range(1, 8)))
    return df


def select_emep_daily_factors(df, timestamp, normalize=False):
    """Select only the requested day from daily factor DataFrame"""
    if normalize:
        weights = df.copy()
        for col in ['country_emep', 'sect_emep']:
            weights.drop(col, axis=1, inplace=True)
        weights = weights.sum(axis=1).values / 7.0
    df['weight_weekly'] = df[timestamp.weekday() + 1]
    for day in range(1, 8):
        df.drop(day, axis=1, inplace=True)

    if normalize:
        df['weight_weekly'] /= weights

    return df


def prepare_emep_daily_factors(path, country_map, timestamp, normalize,
                               extra_countries=None):
    """Prepare EMEP daily factors for use"""
    # which files are there?
    fns = glob.glob(os.path.join(path, 'DailyFac.*'))

    # which species are in these files?
    species = [os.path.splitext(f_)[1][1:].upper() for f_ in fns]

    d_factors = []
    for sp_, fn_ in zip(species, fns):
        df = load_emep_daily_factors(fn_)
        df = select_emep_daily_factors(df, timestamp, normalize)
        df = convert_countries_emep_to_tno(df, country_map, extra_countries)
        df = convert_sectors_emep_to_tno(df)
        df['species'] = sp_
        d_factors.append(df)

    d_factors = pd.concat(d_factors)

    return d_factors


def add_emep_daily_factors(emis, d_factors, timestamp):
    # TODO make distinction between different sources of daily factors

    # species mapping for d_factors
    t_factors = d_factors.merge(
        SPECIES_MAP_EMEP_TO_TNO, left_on='species', right_on='species_emep')
    del t_factors['species']
    t_factors = t_factors.rename(columns={'species_tno': 'species'})
    t_factors = t_factors.sort_values(by=['sect', 'species', 'emep_iso3'])

    # for each emission source find which local hour corresponds to a given
    # UTC hour
    emis['ix_weekday'] = timestamp.isoweekday()

    # species_data = set(emis['species'].unique())
    # species_fact = set(t_factors['species'].unique())
    # if species_data != species_fact:
    #     warnings.warn(
    #         "Emission dataset includes the species {}, while daily "
    #         "factors have {}".format(species_data, species_fact))

    fact_day = emis.merge(
        t_factors,
        left_on=['sect', 'species', 'country', ],
        right_on=['sect', 'species', 'emep_iso3'])

    check_time_factor_apply(emis, fact_day, 'daily')

    return fact_day


def check_time_factor_apply(emis_orig, emis_new, kind):
    if kind not in ('hourly', 'daily', 'monthly'):
        raise ValueError()
    delta_emis = emis_orig.shape[0] - emis_new.shape[0]
    if delta_emis:
        sect_before = sorted(emis_orig['sect'].unique())
        sect_after = sorted(emis_new['sect'].unique())
        country_before = sorted(emis_orig['country'].unique())
        country_after = sorted(emis_new['country'].unique())
        logging.getLogger().warn(
            "Lost {} emission sources while applying {} factors "
            "(sectors before: {}; sectors after: {}, "
            "countries before: {}; countries after: {})".format(
                delta_emis, kind, sect_before, sect_after, country_before,
                country_after))


# GENERAL HELPER FUNCTIONS
# ========================

def prepare_country_map_iso2_to_iso3(
        ser, extra_countries=None, ignore_errors=False):
    """Prepare a country mapping from EMEP ISO2 code to ISO3 code

    Since the EMEP ISO2 code contains some special, "non-country"
    regions, the ``extra_countries`` kwarg gives the possibility to
    force adding these to the mapping, e.g., "NOS -> NOS", so that
    these "countries" don't get filtered out when applying the
    temporal factors.

    """
    iso2 = ser.unique()
    iso3 = []
    for cc in iso2:
        try:
            iso3.append(pycountry.countries.get(alpha_2=cc).alpha_3)
        except (KeyError, AttributeError):
            if cc in extra_countries:
                iso3.append(cc)
            elif ignore_errors:
                iso3.append(None)
            else:
                raise
    return pd.DataFrame({'iso2': iso2, 'iso3': iso3})


def read_emep_country_map(path, extra_countries=None, replacement_values=None):
    df = pd.read_csv(
        path, comment='#', sep='\t',
        names=['emep_iso2', 'emep_country_id', 'emep_country_name'])
    df['emep_country_id'] = df['emep_country_id'].astype(int)

    if replacement_values:
        for key, val in replacement_values.items():
            for kk, vv in val.items():
                ixx = df[key] == kk
                df.loc[ixx, key] = vv

    if extra_countries is None:
        extra_countries = []
    elif isinstance(extra_countries, str):
        extra_countries = [extra_countries]
    country_map = prepare_country_map_iso2_to_iso3(
        df['emep_iso2'], extra_countries=extra_countries, ignore_errors=True)

    df = df.merge(country_map, left_on='emep_iso2', right_on='iso2')
    df['emep_iso3'] = df['iso3'].copy()
    for col in ['iso2', 'iso3']:
        df.drop(col, axis=1, inplace=True)
    return df
