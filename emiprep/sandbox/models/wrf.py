import datetime
import getpass
import logging
import os.path
import socket

from netCDF4 import Dataset
import pandas as pd
import xarray as xr
import wrf

from emiprep import __version__
from ...regrid.cdo import metgrid_to_cdo_griddes, read_griddes
from ..util import mktempfn


# global variables which should be present in wrfchemi files, according to
# https://ruc.noaa.gov/wrf/wrf-chem/wrf_tutorial_2017/Best_Practices.pdf
ATTRS_WRFCHEMI = [
    'SIMULATION_START_DATE', 'SIMULATION_INITIALIZATION_TYPE',
    'WEST-EAST_GRID_DIMENSION', 'SOUTH-NORTH_GRID_DIMENSION',
    'BOTTOM-TOP_GRID_DIMENSION', 'DX', 'DY', 'GRID_ID', 'PARENT_ID',
    'I_PARENT_START', 'J_PARENT_START', 'PARENT_GRID_RATIO', 'DT', 'CEN_LAT',
    'CEN_LON', 'TRUELAT1', 'TRUELAT2', 'MOAD_CEN_LAT', 'STAND_LON',
    'POLE_LAT', 'POLE_LON', 'GMT', 'JULYR', 'JULDAY', 'MAP_PROJ',
    'MAP_PROJ_CHAR', 'MMINLU', 'NUM_LAND_CAT', 'ISWATER', 'ISLAKE', 'ISICE',
    'ISURBAN', 'ISOILWATER', 'HYBRID_OPT', 'ETAC']

ATTRS_EMIS = {
    'FieldType': 104,
    'MemoryOrder': 'XYZ',
    'description': 'EMISSIONS',
    'units': 'mol km^-2 hr^-1',  # TODO make this string depend on aer/nonaer
    'stagger': '',
    'coordinates': 'XLONG XLAT'
    }

log = logging.getLogger('emiprep')


def read_output_definition(path):
    """Read required global attributes needed from ``wrfinput`` file
    """
    log.debug("  Read wrfinput attributes")
    with Dataset(path, 'r') as nc:
        keys = [a_ for a_ in nc.ncattrs() if a_ in ATTRS_WRFCHEMI]
        attrs = {a_: getattr(nc, a_) for a_ in keys}
    return attrs


def read_output_grid(path):
    log.debug("  Create WRF griddes")
    grid = metgrid_to_cdo_griddes(path)
    return read_griddes(grid)


def read_model_vertical_structure(path):
    """Read altitude of model layer interfaces from ``wrfinput`` file

    Notes
    -----

    This function does not use ``wrf.getvar('height')`` because we
    need the height at level interfaces.  Here we follow the advice
    given at https://github.com/NCAR/wrf-python/issues/20.

    """
    log.debug("  Read levels from wrfinput")
    with Dataset(path, 'r') as nc:
        var_ph = wrf.getvar(nc, 'PH')
        var_phb = wrf.getvar(nc, 'PHB')
        h_surf = wrf.getvar(nc, 'HGT')
    z = (var_ph + var_phb) / wrf.constants.Constants.G
    h = z - h_surf
    return h


def _time_to_timestr(arr):
    return ['{time:%Y-%m-%d_%H:%M:%S}'.format(time=t).encode('ascii')
            for t in pd.DatetimeIndex(arr.values)]


def _rename_datestrlen_dimension(path_in, path_out, deflate):
    # TODO use the same netCDF4 options for output file
    with Dataset(path_in, 'r') as ncin, Dataset(path_out, 'w') as ncout:

        for attname in ncin.ncattrs():
            setattr(ncout, attname, getattr(ncin, attname))

        for dimname, dim in ncin.dimensions.items():
            if dimname != 'string19':
                ncout.createDimension(dimname, len(dim))
        ncout.createDimension('DateStrLen', 19)

        for varname, ncvar in ncin.variables.items():
            if varname.startswith('__'):  # added by xarray, not needed by WRF
                continue
            dims_new = [dim if dim != 'string19' else 'DateStrLen'
                        for dim in ncvar.dimensions]
            var = ncout.createVariable(
                varname, ncvar.dtype, dims_new,
                zlib=deflate, complevel=6, fletcher32=True)
            for attname in ncvar.ncattrs():
                try:
                    setattr(var, attname, getattr(ncvar, attname))
                except AttributeError:
                    pass
            var[:] = ncvar[:]


def _create_wrfchemi_iostyle1(emis, path, timestep, deflate):
    # TODO cannot import top-level due to circular dependency
    from ...config import cfg

    # select subset for the actual day, even though it's not necessary for WRF
    if 'Time' not in emis.dims:
        log.warn(
            "Emission Dataset contains only one time step instead of 24!")
        log.debug("Adding 1-D time coordinate")
        emis = xr.concat([emis.assign_coords(Time=cfg.time.start)], 'Time')
    else:
        if emis.dims['Time'] != 24:
            log.warn(
                "Emission Dataset contains {} time steps instead of 24!"
                "".format(emis.dims['Time']))
    if not emis['Times'].values[0].decode().endswith('00:00:00'):
        log.warn(
            "Emission Dataset's first timestep is at {} instead of "
            "00:00:00.".format(emis['Times'].values[0].decode()))
    fn = os.path.split(path)[1]
    if not fn.startswith('wrfchemi_') or fn.find('_{:02d}z_d0') == -1:
        log.warn(
            "Unexpected output filename.  Usually, path should start with "
            "`wrfchemi_` and contain the string `_{:02d}z_d0`")

    tmpname = mktempfn()

    emis.isel(Time=slice(0, 12)).to_netcdf(
        tmpname, encoding={'Times': {'dtype': 'S1'}})
    _rename_datestrlen_dimension(tmpname, path.format(0), deflate)
    os.remove(tmpname)

    emis.isel(Time=slice(12, 24)).to_netcdf(
        tmpname, encoding={'Times': {'dtype': 'S1'}})
    _rename_datestrlen_dimension(tmpname, path.format(12), deflate)
    os.remove(tmpname)


def _create_wrfchemi_iostyle2(emis, path, timestep, domain,
                              deflate=False, nocolons=True):
    if emis.dims['Time'] != 24:
        log.warn(
            "Emission Dataset contains {} time steps instead of 24!"
            "".format(emis.dims['Time']))
    if not emis['Times'].values[0].decode().endswith('00:00:00'):
        log.warn(
            "Emission Dataset's first timestep is at {} instead of "
            "00:00:00.".format(emis['Times'].values[0].decode()))

    # setup path of final output file
    dirname, fname = os.path.split(path)
    for marker in ['<date>', '<domain>']:
        if fname.find(marker) < 0:
            raise ValueError(
                "Cannot find placeholder '{}' in path".format(
                    marker))
    fname_tpl = fname.replace('<domain>', '{domain:02d}')
    if nocolons:
        fname_tpl = fname_tpl.replace('<date>', '{timestep:%Y-%m-%d_%H_%M_%S}')
    else:
        fname_tpl = fname_tpl.replace('<date>', '{timestep:%Y-%m-%d_%H:%M:%S}')
    fname = fname_tpl.format(domain=domain, timestep=timestep)

    tmpname = mktempfn()

    emis.to_netcdf(
        tmpname, encoding={'Times': {'dtype': 'S1'}})
    _rename_datestrlen_dimension(
        tmpname, os.path.join(dirname, fname), deflate)
    os.remove(tmpname)


def create_output(emis, path, timestep, domain, io_style, output_attrs,
                  deflate=False, nocolons=False):
    """Create WRF/Chem emission input files

    Parameters
    ----------


    io_style : int

        Style of anthropogenic emissions for WRF/Chem should be
        produced.  This argument is equivalent to the
        ``io_style_emissions`` namelist entry:

        - 1 is for hourly emissions data (``wrfchemi_00z_dXX`` and
          ``wrfchemi_12z_dXX``)
        - 2 is for daily varying emissions data
          (``wrfchemi_dXX_TIMESTAMP``)

    TODO
    ----
    - Check for existing files and give option to overwrite (or not) them
    - Support io_style=2
    - add global attributes

    """
    # input validation
    if io_style not in [1, 2]:
        raise ValueError()
    if not isinstance(timestep, datetime.datetime):
        raise ValueError()

    # check provided global attributes
    for key in ATTRS_WRFCHEMI:
        if key not in output_attrs.keys():
            log.warn(
                "Required output attribute '{}' not found".format(key))
    for key in list(output_attrs.keys()):
        if key not in ATTRS_WRFCHEMI:
            output_attrs.pop(key)

    # set global atttributes
    output_attrs['comment'] = 'Created by emiprep {} on {} by {}@{}'.format(
        __version__, datetime.datetime.now().isoformat(),
        getpass.getuser(), socket.getfqdn())

    emis = emis.rename({'xdim': 'west_east', 'ydim': 'south_north',
                        'zdim': 'emissions_zdim', 'time': 'Time'})
    emis['Times'] = xr.DataArray(
        _time_to_timestr(emis['Time']), coords={'Time': emis['Time']},
        dims=['Time'])

    for k, v in output_attrs.items():
        emis.attrs[k] = v

    try:
        emis = emis.transpose(
            ('Time', 'emissions_zdim', 'south_north', 'west_east'))
    except ValueError:
        pass

    # add needed attributes on data variables
    for var in emis.variables:
        if var == 'Times':
            continue
        for k, v in ATTRS_EMIS.items():
            emis[var].attrs[k] = v

    emis = emis.sel(Time='{:%Y-%m-%d}'.format(timestep))

    for k in ['Time', 'emissions_zdim', 'west_east', 'south_north']:
        del emis[k]

    if io_style == 1:
        _create_wrfchemi_iostyle1(emis, path, timestep, deflate)
    elif io_style == 2:
        _create_wrfchemi_iostyle2(
            emis, path, timestep, domain, deflate, nocolons)


create_wrfchemi = create_output
