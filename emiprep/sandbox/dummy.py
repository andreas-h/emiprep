import numexpr
import numpy as np
import pandas as pd


numexpr.set_num_threads(1)  # see dd.DataFrame.query() docstring


def load_time_factors(emis):
    cy_d = load_time_factor_diurnal(emis)
    cy_w = load_time_factor_weekly(emis)
    cy_m = load_time_factor_monthly(emis)
    cy = dict(daily=cy_d, weekly=cy_w, monthly=cy_m)
    return cy


def load_time_factor_diurnal(emis):
    # weekly cycle
    countries = emis.country.unique().compute().tolist()
    species = emis.species.unique().compute().tolist()
    sectors = emis.sect.unique().compute().tolist()
    hour = np.arange(24, dtype=int)

    ix_d = pd.MultiIndex.from_product((countries, sectors, species, hour))
    cy_d = pd.DataFrame(ix_d.to_frame().values,
                        columns=['country', 'sect', 'species', 'hour'])
    cy_d['weight_daily'] = 1.0
    return cy_d


def load_time_factor_weekly(emis):
    # weekly cycle
    countries = emis.country.unique().compute().tolist()
    species = emis.species.unique().compute().tolist()
    sectors = emis.sect.unique().compute().tolist()
    weekday = np.arange(1, 8, dtype=int)

    ix_w = pd.MultiIndex.from_product((countries, sectors, species, weekday))
    cy_w = pd.DataFrame(ix_w.to_frame().values,
                        columns=['country', 'sect', 'species', 'weekday'])
    cy_w['weight_weekday'] = 1.0
    return cy_w


def load_time_factor_monthly(emis):
    # weekly cycle
    countries = emis.country.unique().compute().tolist()
    species = emis.species.unique().compute().tolist()
    sectors = emis.sect.unique().compute().tolist()
    month = np.arange(1, 13, dtype=int)

    ix_m = pd.MultiIndex.from_product((countries, sectors, species, month))
    cy_m = pd.DataFrame(ix_m.to_frame().values,
                        columns=['country', 'sect', 'species', 'month'])
    cy_m['weight_month'] = 1.0
    return cy_m
