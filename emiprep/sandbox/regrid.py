import logging

import dask.dataframe as dd
from numba import njit, prange
import numpy as np
import pandas as pd
import xarray as xr

from ..config import cfg
from ..regrid.cdo import (
    calc_gridcell_area, prepare_remap_weights, get_srcgrid_index, get_xy_index)


log = logging.getLogger('emiprep')


class VerticalDistribution(object):

    def _calc_vertical_overlap(self, z_model):
        model_layer = range(z_model.shape[0] - 1)
        emis_layer = self.layer_weights.index

        factor = np.zeros((len(emis_layer), ) +
                          (z_model.shape[0] - 1, ) +
                          z_model.shape[1:])

        for ml in model_layer:
            for el in emis_layer:
                if (z_model[ml].values.min() >
                        self.layer_weights['z_max'].max()):
                    break
                upper = np.minimum(z_model[ml + 1].values,
                                   self.layer_weights['z_max'].iloc[el])
                lower = np.maximum(z_model[ml].values,
                                   self.layer_weights['z_min'].iloc[el])
                overlap = np.maximum(upper - lower, 0.)
                factor[el, ml] = (
                    overlap / (self.layer_weights['z_max'].iloc[el] -
                               self.layer_weights['z_min'].iloc[el]))

        if not np.allclose(
                factor.sum(axis=0).sum(axis=0), emis_layer.max() + 1):
            raise ValueError()

        factor = factor.reshape(factor.shape[:2] + (-1, )).transpose((2, 1, 0))
        return factor

    def add_vertical_weights_to_emis(self, emis, sector_mapping, z_model):
        # `overlap` cannot be da.Array, otherwise overlap[...] below blows up
        log.debug("  Calculate overlap")
        # FIXME reduce z_model to needed values
        overlap = self._calc_vertical_overlap(z_model)

        # index of column (in weights DF) containing the for each sector from
        # inventory `emis`
        log.debug("  Find matching layers")
        sect_emis2vert_ix = {k: self.layer_weights.columns.tolist().index(v)
                             for k, v in sector_mapping.items()}
        ix_sect = emis['sect'].apply(lambda x: sect_emis2vert_ix[x])

        # add this index to our local working copy of the emissions DF
        emis['ix_sect'] = ix_sect

        # prepare 1D weight arrays for the emissions DF: one for the overlap
        # weights, and one for the emission distribution weights
        log.debug("  Prepare arrays for merge operation")
        vert_left = overlap[emis['ix_dstgrid'].values - 1]
        vert_right = self.layer_weights.values[:, ix_sect].T

        # calculate the cumulative vertical distribution weight for each
        # emission source
        log.debug("  Calculate weights")

        @njit(parallel=True)
        def _create_vert_weights_dst(vert_left, vert_right):
            vert_weights_dst = np.empty(
                (vert_left.shape[0], vert_left.shape[1]), dtype=np.float32)
            for ii in prange(vert_left.shape[0]):
                vert_weights_dst[ii] = np.dot(vert_left[ii], vert_right[ii])
            return vert_weights_dst
        vert_weights_dst = pd.DataFrame(
            _create_vert_weights_dst(vert_left, vert_right))
        del vert_left, vert_right

        vert_weights_dst.insert(0, 'ix_regridded', vert_weights_dst.index)

        # rearrange the cumulative weights into a mergeable DF
        log.debug("  Rearrange cumulative weights")
        vert_weights_dst = vert_weights_dst.melt(
            id_vars=['ix_regridded'], value_name='weight_vert',
            var_name='ix_dstlev')

        # filter out zero values
        log.debug("  Filter out zero values")
        ix_nonzero = (vert_weights_dst['weight_vert'] != 0.)
        vert_weights_dst = vert_weights_dst[ix_nonzero]

        # add the weights (and index) to the emissions DF
        log.debug("  Add vertical regridding weights to emissions")
        em = emis.merge(vert_weights_dst, on='ix_regridded')

        em.insert(3, 'ix_vertdist', em.index)

        return em


class EMEPVerticalDistribution(VerticalDistribution):
    # TODO add reference for EMEP vertical weights
    # vertical layering of the EMEP data
    emis_heights = [0., 92., 184., 324., 522., 781., 1106]
    zmin, zmax = emis_heights[:-1], emis_heights[1:]

    layer_weights = pd.DataFrame()
    layer_weights['z_min'] = zmin
    layer_weights['z_max'] = zmax
    layer_weights['SNAP1'] = [0., 0., .15, .4, .3, .15]
    layer_weights['SNAP2'] = [1., 0., 0., 0., 0., 0.]
    layer_weights['SNAP3'] = [.1, .1, .15, .3, .3, .05]
    layer_weights['SNAP4'] = [.9, .1, 0., 0., 0., 0.]
    layer_weights['SNAP5'] = [.9, .1, 0., 0., 0., 0.]
    layer_weights['SNAP6'] = [1., 0., 0., 0., 0., 0.]
    layer_weights['SNAP7'] = [1., 0., 0., 0., 0., 0.]
    layer_weights['SNAP8'] = [1., 0., 0., 0., 0., 0.]
    layer_weights['SNAP9'] = [.1, .15, .4, .35, .0, .0]
    layer_weights['SNAP10'] = [1., 0., 0., 0., 0., 0.]

    if not (layer_weights[[c for c in layer_weights.columns
                           if c.startswith('SNAP')]].sum() == 1.).all():
        raise ValueError()


def interpolate_inventory(emis, grid_inventory, grid_output):
    """Horizontally interpolate emissions from inventory to target grid
    """
    # calculate grid cell area of inventory
    # TODO optionally provide Earth radius
    # TODO what do we need the inventory area here?
    log.debug("  Calculate gridcell area")
    output_area = calc_gridcell_area(grid_output)
    output_area = pd.DataFrame(
        {'ix_dstgrid': np.arange(1, output_area.size + 1, dtype=int),
         'gridarea_dst': output_area.ravel()})

    # calculate remapping weights
    # TODO read n_cpu for CDO from global settings

    # TODO use the inventory class for calc_remap_weights in case of
    #      TNO inventory which is given in t yr-1

    # we need to know the fraction of each inventory grid cell which
    # falls into each touched target grid cell, so that for the target
    # grid cell we can just sum up the contributions from all touching
    # inventory grid cells.  Therefore we need to calculate the
    # remap_weights 'backwards' here, which is being taken care of by
    # `prepare_remap_weights`.
    log.debug("  Prepare remapping weights")
    remap_weights = prepare_remap_weights(
        grid_inventory, grid_output, n_cpu=cfg.runtime.nproc, norm='destarea')

    # add CDO src_address to emissions DF
    log.debug("  Add src_address to emissions DataFrame")
    em = emis.compute()
    ix_src = get_srcgrid_index(em['ix_lon'], em['ix_lat'], grid_inventory)
    em['ix_srcgrid'] = ix_src

    # handle point and area sources seperately
    log.debug("  Split area and point sources")
    em_p = em[em['a_p'] == 'P'].copy()
    em_a = em[em['a_p'] == 'A'].copy()
    del em

    # AREA SOURCES

    # NOTE Even if there are no area sources, this code will run and
    #      produce an empty `em_a`.
    log.debug("  Apply remapping weights to area sources")
    em_a = em_a.merge(remap_weights)

    # add ix_dstlon/lat to em_a
    output_xsize = grid_output['xsize']
    ix_dstlat = (em_a['ix_dstgrid'] - 1) // output_xsize
    ix_dstlon = (em_a['ix_dstgrid'] - 1) % output_xsize
    em_a['ix_dstlon'] = ix_dstlon
    em_a['ix_dstlat'] = ix_dstlat
    em_a.reset_index(inplace=True)

    # POINT SOURCES

    # NOTE If there are no point sources, an `IndexError` will be
    #      thrown by `get_xy_index`.  In that case we need to make
    #      sure `em_p` is a DataFrame which can be properly
    #      concatenated to `em_a`.
    try:
        # lon, lat indices of point sources in target grid
        log.debug("  Locate point sources in output grid")
        ix_dst_x, ix_dst_y = get_xy_index(
            em_p['lon'], em_p['lat'], grid_output)
        # CDO address starts at 1
        ix_dstgrid = ix_dst_x + ix_dst_y * output_xsize + 1
        em_p.loc[:, 'ix_dstgrid'] = ix_dstgrid
        em_p.loc[:, 'ix_dstlon'] = ix_dst_x
        em_p.loc[:, 'ix_dstlat'] = ix_dst_y
        # filter out-of-bounds point sources, which have ix_dstlon/lat = -1
        log.debug("  Filter out-of-bounds point sources")
        em_p = em_p[em_p['ix_dstlon'] > -1]
        em_p.reset_index(inplace=True)

        # point sources in kg hr-1 translate from inventory to target
        # grid with weight 1.0
        # TODO what about other input units?
        em_p['weight'] = 1.0
    except IndexError:  # no point sources
        log.info("There seem to be no point sources")
        em_p['ix_dstgrid'] = np.empty((0, ), dtype=int)
        em_p['ix_dstlon'] = np.empty((0, ), dtype=int)
        em_p['ix_dstlat'] = np.empty((0, ), dtype=int)
        em_p['weight'] = np.empty((0, ), dtype=float)

    # Merge area and point sources together again
    log.debug("  Merge point and area sources")
    if isinstance(em_p, dd.DataFrame):
        em = em_p.append(em_a)
    elif isinstance(em_a, dd.DataFrame):
        em = em_a.append(em_p)
    else:
        em = pd.concat((em_a, em_p), axis=0, sort=False, copy=False)

    log.debug("  Quality control of weighting factors")
    if em['weight'].max() > 1.0000001 or em['weight'].min() < 0.0:
        log.error(
            "Horizontal re-gridding lead to unreasonable weighting factors.  "
            "Aborting.")
        raise ValueError

    # Add a new index column, for traceability
    log.debug("  Add new index column for traceability.")
    em.insert(4, 'ix_regridded', np.arange(em.shape[0]))

    em = em.rename(columns={'weight': 'weight_regrid'})

    # Add output grid cell area to DataFrame
    log.debug("  Add grid cell area")
    em = em.merge(output_area)

    return em


def regrid(emis_orig, griddef_inventory, output_def_hor, output_def_vert,
           vertical_distribution, vertical_sector_mapping):
    log.debug("Horizontal regridding")
    emis_interpolated = interpolate_inventory(
        emis_orig, griddef_inventory, output_def_hor)

    log.debug("Vertical regridding")
    emis_distributed = vertical_distribution.add_vertical_weights_to_emis(
        emis_interpolated, vertical_sector_mapping, output_def_vert)

    return emis_distributed


def grid_emissions(df, griddes, t, species_list):
    nx, ny = griddes['xsize'], griddes['ysize']

    # convert from kg h-1 to kg h-1 m-2
    log.debug("Convert units")
    df['emis'] = df['emis'] / df['gridarea_dst']

    log.debug("Aggregate emissions per grid cell")
    df.set_index(['species_new', 'ix_dstlev', 'ix_dstlat', 'ix_dstlon'],
                 inplace=True)
    log.debug("Aggregate emissions per grid cell")
    emser = df['emis'].groupby(
        ['species_new', 'ix_dstlev', 'ix_dstlat', 'ix_dstlon']).sum()

    log.debug("Convert to xr.DataArray")
    log.debug("Fill index")

    xdimnew, ydimnew = np.arange(nx), np.arange(ny)
    zdimnew = np.arange(emser.reset_index()['ix_dstlev'].max() + 1)

    log.debug("MultiIndex")
    ix_new = pd.MultiIndex.from_product(
        (emser.reset_index()['species_new'].unique(), zdimnew, ydimnew,
         xdimnew),
        names=['species_new', 'zdim', 'ydim', 'xdim'])
    emser = emser.reindex(ix_new).fillna(0.)
    log.debug("to_xarray")
    try:
        em = emser.compute().to_xarray().to_dataset('species_new')
    except AttributeError:
        em = emser.to_xarray().to_dataset('species_new')
    del emser

    # Pad Dataset to target grid dimensions
    log.debug("Pad Dataset to target grid dimensions")
    spvars = [sp for sp in em.variables if not sp.endswith('dim')]
    xdimnew, ydimnew = np.arange(nx), np.arange(ny)
    zdimnew = np.arange(em.dims['zdim'])
    emptyarr = xr.DataArray(
        np.zeros((em.dims['zdim'], ny, nx)),
        coords={'zdim': zdimnew, 'ydim': ydimnew, 'xdim': xdimnew, 'time': t},
        dims=['zdim', 'ydim', 'xdim'])
    emptyds = xr.Dataset({k: emptyarr for k in spvars})

    ds = em.combine_first(emptyds)

    return ds
