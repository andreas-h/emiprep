import logging

import dask.dataframe as dd
import numpy as np
import pandas as pd

from ...config import cfg
from ...regrid.cdo import get_xy_index


log = logging.getLogger('emiprep')


def prepare_inventory(path, griddes):
    # TODO re-factor this into Inventory classes

    # read inventory into pandas DataFrame

    em = pd.read_csv(path, sep=';')

    # rename columns
    em = em.drop('Year', axis=1)
    em = em.rename(
        columns={'Lon': 'lon', 'Lat': 'lat', 'ISO3': 'country', 'SNAP': 'sect',
                 'SourceType': 'a_p'})
    em.insert(0, 'ix_orig', em.index)

    # PM10 column is PM2_5 + (PM10 - PM2_5) -> convert to PM_COARSE
    # and PM_FINE, and make sure PM_COARSE is >= 0
    em = em.rename(columns={'PM2_5': 'PM_FINE'})
    em['PM_COARSE'] = em['PM10'] - em['PM_FINE']
    em.loc[em['PM_COARSE'] < 0., 'PM_COARSE'] = 0.
    del em['PM10']

    log.debug("Geo-referencing emission sources")
    ix_lon, ix_lat = get_xy_index(em['lon'], em['lat'], griddes)
    em.insert(1, 'ix_lon', ix_lon)
    em.insert(2, 'ix_lat', ix_lat)

    if isinstance(em, pd.DataFrame):
        em = em.melt(
            id_vars=['ix_orig', 'ix_lon', 'ix_lat', 'lon', 'lat', 'country',
                     'sect', 'a_p'],
            var_name='species', value_name='emis_orig')
    elif isinstance(em, dd.DataFrame):
        em = dd.reshape.melt(
            em, id_vars=['ix_orig', 'ix_lon', 'ix_lat', 'lon', 'lat',
                         'country', 'sect', 'a_p'],
            var_name='species', value_name='emis_orig')

    # add index for original (speciated) emission source
    em.insert(1, 'ix_emis', np.arange(em.shape[0]))
    em.set_index('ix_emis')

    # TODO ??? change to categorical dtype to save memory
    # em = em.astype(
    #     {key: 'category' for key in [
    #         'country', 'sect', 'a_p', 'species']}) #, 'lon', 'lat']})

    # remove empty lines
    em = em[em['emis_orig'] != 0.]

    em = dd.from_pandas(em, chunksize=cfg.runtime.dask_bs)
    return em
