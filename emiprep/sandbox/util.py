import logging
import tempfile

from ..config import cfg


log = logging.getLogger('emiprep')


def mktempfn():
    _, tmpfn = tempfile.mkstemp(dir=cfg.runtime.get('tmpdir'))
    return tmpfn
