"""emiprep.cycles.emep
===================

This module contains methods to handle time factors for emissions from
the EMEP_ model.

The original input data files are described in Section 2.2.4 of the
`EMEP User Guide
<https://github.com/metno/emep-ctm/blob/docs/User_Guide.pdf>`__

.. currentmodule:: emiprep.cycles.emep

.. autosummary::
   :toctree: api/

   read_countries
   read_daily_factors
   read_hourly_factors
   read_monthly_factors

.. _EMEP: https://wiki.met.no/emep/page1/emepmscw_opensource

"""

from __future__ import (
    absolute_import, division, print_function, unicode_literals)

from builtins import open, range
from itertools import product

import numpy as np
import pandas as pd


def read_hourly_factors(fn):
    d = pd.read_csv(
        fn, sep=' ', comment='#',
        names=['day', 'snap'] + ['f{:02d}'.format(i + 1) for i in range(24)])
    # f01 is from 00:00 to 00:59:59.999999, according to EMEP file header
    days = list(range(7))  # 0=Monday, 6=Sunday, see pd.DatetimeIndex.weekday
    hours = list(range(24))
    tuples = list(product(days, hours))
    ix = pd.MultiIndex.from_tuples(tuples, names=['day', 'hour'])
    snap_sectors = d['snap'].unique()

    df = pd.DataFrame(index=ix, columns=snap_sectors)
    df.columns.name = 'snap'

    for day in range(7):
        for snap in snap_sectors:
            ix_day = d['day'] == day + 1  # TODO Check if EMEP_day==1 -> Monday
            ix_snap = d['snap'] == snap
            df.loc[(day, ), snap] = d[ix_day & ix_snap].iloc[0, 2:].values

    return df


def read_countries(fn):
    # original data file isn't consistent in field separators, so we
    # read manually ...
    with open(fn, 'r') as fd:
        lines = fd.readlines()
    iso, n, names = [], [], []
    for l in lines:
        if l.startswith('#') or l.startswith('ISO2'):
            continue
        iso.append(l.split()[0])
        n.append(l.split()[1])
        names.append(' '.join(l.split()[2:]))
    df = pd.DataFrame({'n': n, 'country_name': names}, index=iso)
    df['n'] = df['n'].astype(np.int)
    return df


def _prepare_time_factors(df_in, ix_out, snap_sectors, countries):

    df_out = pd.DataFrame(index=ix_out, columns=snap_sectors)
    df_out.sort_index(inplace=True)
    df_out[:] = 1.  # default value in case EMEP doesn't specify factors
    df_out.columns.name = 'snap'

    # sort data into DataFrame
    for c, snap in product(countries.index.unique(), snap_sectors):
        ix_country = (df_in['country'] == countries.loc[c, 'n'])
        ix_snap = (df_in['snap'] == snap)

        try:  # not all country/snap combinations have data
            # in some cases, EMEP weights have a mean != 1.0 for a
            # given country/snap pair.  So we normalize the EMEP data
            # appropriately.
            weight = df_in[ix_country & ix_snap].iloc[0, 2:].mean()
            df_out.loc[(c, ), snap] = (
                df_in[ix_country & ix_snap].iloc[0, 2:].values / weight)
        except IndexError:
            pass

    return df_out


def _sanity_check_time_factors(df, total_weight, snap_sectors, countries):
    # sanity check
    for c, snap in product(countries.index.unique(), snap_sectors):
        if not np.allclose(df.loc[(c, ), snap].sum(), total_weight):
            raise ValueError("Error while reading daily factors.")


def read_monthly_factors(fn, fn_countries):
    # read EMEP daily factors
    d = pd.read_csv(
        fn, delim_whitespace=True,
        names=['country', 'snap', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
               'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])

    # prepare output DataFrame
    countries = read_countries(fn_countries)
    months = np.arange(1, 13)
    tuples = list(product(countries.index, months))
    ix = pd.MultiIndex.from_tuples(tuples, names=['country', 'month'])
    snap_sectors = d['snap'].unique()

    df = _prepare_time_factors(d, ix, snap_sectors, countries)

    _sanity_check_time_factors(df, 12.0, snap_sectors, countries)

    return df


def read_daily_factors(fn, fn_countries):
    # read EMEP daily factors
    d = pd.read_csv(
        fn, delim_whitespace=True,
        names=['country', 'snap', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
               'Sun'])

    # prepare output DataFrame
    countries = read_countries(fn_countries)
    days = list(range(7))  # 0=Monday, 6=Sunday, see pd.DatetimeIndex.weekday
    tuples = list(product(countries.index, days))
    ix = pd.MultiIndex.from_tuples(tuples, names=['country', 'day'])
    snap_sectors = d['snap'].unique()

    df = _prepare_time_factors(d, ix, snap_sectors, countries)

    _sanity_check_time_factors(df, 7.0, snap_sectors, countries)

    return df
