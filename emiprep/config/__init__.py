import yaml

import pandas as pd
import xarray as xr


class _confval(dict):
    # from https://goodcode.io/articles/python-dict-object/
    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: " + name)


def _prepare_confvals(vals):
    if isinstance(vals, list):
        return [_prepare_confvals(v) for v in vals]
    elif isinstance(vals, dict):
        return _confval({k: _prepare_confvals(v) for k, v in vals.items()})
    else:
        return vals


# from https://stackoverflow.com/a/6798042
class _Singleton(type):
    """ A metaclass that creates a Singleton base class when called. """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(_Singleton, cls).__call__(*args,
                                                                  **kwargs)
        return cls._instances[cls]


class Singleton(_Singleton('SingletonMeta', (object,), {})):
    pass


class Config(Singleton):

    def load(self, path):
        with open(path, 'r') as fd:
            vals = _prepare_confvals(yaml.safe_load(fd))
            for k, v in vals.items():
                self.__setattr__(k, v)

    def prepare(self):
        # don't use top-level import to avoid circular dependency
        from ..sandbox.models.wrf import (
            read_model_vertical_structure, read_output_grid)

        # prepare values
        self.time.start = pd.Timestamp(self.time.start)
        self.time.end = pd.Timestamp(self.time.end)
        self.time.delta_t = pd.Timedelta(self.time.delta_t)
        self.model.domain = int(self.model.domain)
        self.model.io_style = int(self.model.io_style)
        self.output.ncattrs = xr.open_dataset(
            self.model.path_wrfinput.format(
                domain=self.model.domain)).attrs.copy()
        self.model.output_def_hor = read_output_grid(
            self.model.path_metgrid.format(domain=self.model.domain))
        self.model.output_def_vert = read_model_vertical_structure(
            self.model.path_wrfinput.format(domain=self.model.domain))
        self.model.species_definition.data = pd.read_csv(
            self.model.species_definition.path.format(
                chem_opt=self.model.chem_opt), comment='#')


cfg = Config()
