"""emiprep.util
============

.. currentmodule:: emiprep.util

.. autosummary::
   :toctree: api/

   modified_environ

"""

from __future__ import (
    absolute_import, division, print_function, unicode_literals)

import contextlib
import os


@contextlib.contextmanager
def modified_environ(*remove, **update):
    """Temporarily update the ``os.environ`` dictionary in-place.

    The ``os.environ`` dictionary is updated in-place so that the modification
    is sure to work in all situations.

    Parameters
    ----------
    *remove : list
        Environment variables to remove.

    **update : dict
        Dictionary of environment variables and values to add/update.

    References
    ----------
    This function was copied from https://stackoverflow.com/a/34333710/152439.

    """
    env = os.environ
    update = update or {}
    remove = remove or []

    # List of environment variables being updated or removed.
    stomped = (set(update.keys()) | set(remove)) & set(env.keys())
    # Environment variables and values to restore on exit.
    update_after = {k: env[k] for k in stomped}
    # Environment variables and values to remove on exit.
    remove_after = frozenset(k for k in update if k not in env)

    try:
        env.update(update)
        for k in remove:
            env.pop(k, None)
        yield
    finally:
        env.update(update_after)
        for k in remove_after:
            env.pop(k)
