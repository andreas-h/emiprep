import logging

from ..config import cfg


def _setup_logging():
    log = logging.getLogger('emiprep')

    if cfg.logging.level.upper() == 'DEBUG':
        level = logging.DEBUG
    else:
        raise NotImplementedError()

    log.setLevel(level)

    # Create the Handler for logging data to a file
    # logger_handler = logging.FileHandler('python_logging.log')
    logger_handler_stream = logging.StreamHandler()
    logger_handler_stream.setLevel(level)

    logger_handler_file = logging.FileHandler(cfg.logging.filename)
    logger_handler_file.setLevel(level)

    # Create a Formatter for formatting the log messages
    logger_formatter = logging.Formatter(
        '{asctime}  {levelname:5s}  {message}', '%Y-%m-%dT%H:%M:%S', style='{')

    # Add the Formatter to the Handler
    logger_handler_stream.setFormatter(logger_formatter)
    logger_handler_file.setFormatter(logger_formatter)

    # Add the Handler to the Logger
    log.addHandler(logger_handler_stream)
    log.addHandler(logger_handler_file)
