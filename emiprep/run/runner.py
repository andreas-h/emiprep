"""Emiprep runner
=================

This is a temporary run script.  It is a milestone on the path from
the `sandbox/runall.py` script to a configuration file based run.

Execute with the command

    python -m emiprep.run.runner -c emiprep/config/tno_crimech.yaml

from the repository root.

"""

import argparse
import logging
import os.path

import pandas as pd

from ..config import cfg
from ..log import _setup_logging

from .do_preprocess import run_preprocess
from .do_timeloop import run_timeloop


log = logging.getLogger('emiprep')


def _parse_commandline_arguments():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-c', dest='config',
                        help='sum the integers (default: find the max)')
    parser.add_argument('-v', dest='verbose', action='store_true',
                        help='sum the integers (default: find the max)')

    args = parser.parse_args()
    return args


def _setup_dask():
    # FIXME probably we don't want to use a LocalCluster from distributed
    from distributed import Client, LocalCluster
    cluster = LocalCluster(
        n_workers=1, threads_per_worker=cfg.runtime.nproc, processes=False,
        ip='0.0.0.0')
    client = Client(cluster)  # noqa


def read_preprocessed():

    emis = {}

    for inv in cfg.inventories:
        path_pre = cfg.inventories[inv].paths.preprocessed.format(
            domain=cfg.model.domain)

        log.debug(
            "Reading pre-processed inventory data from {}".format(path_pre))

        emis[inv] = pd.read_parquet(path_pre)

    return emis


if __name__ == '__main__':
    args = _parse_commandline_arguments()
    cfg.load(args.config)
    cfg.prepare()
    _setup_logging()

    # TODO properly setup dask
    # _setup_dask()
    log.info('!!! Starting new emiprep run !!!')

    log.info('Loaded configuration from {}'.format(
        os.path.abspath(args.config)))

    if 'preprocess' in cfg.action:
        inventories = run_preprocess()
    else:
        inventories = read_preprocessed()

    if 'timeloop' in cfg.action:
        run_timeloop(inventories)
