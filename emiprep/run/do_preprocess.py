import logging

import dask.dataframe as dd

from ..config import cfg

from ..regrid.cdo import read_griddes

from ..sandbox.inventories import prepare_inventory
from ..sandbox.cycles import add_timezone
from ..sandbox.regrid import regrid, EMEPVerticalDistribution

# TODO conditional import depending on output model type
from ..sandbox.models.wrf import (
    read_model_vertical_structure, read_output_grid)

log = logging.getLogger('emiprep')


def run_preprocess():
    log.info("Start preprocessing")
    log.debug(
        "Inventories to be processed: {}".format(cfg.inventories.keys()))
    inventories = {}
    for inventory in cfg.inventories.keys():
        inventories[inventory] = preprocess_inventory(inventory)
    return inventories


def filter_inventory(emis, criteria=None):
    if isinstance(criteria, str):
        emis = emis.query(criteria)
    elif isinstance(criteria, list):
        for crit in criteria:
            emis = emis.query(crit)
    elif criteria is None:
        pass
    else:
        raise ValueError()
    return emis


def replace_in_inventory(emis, criteria=None):
    if not isinstance(criteria, dict):
        log.error("Cannot replace inventory based on {}".format(criteria))
        return emis
    emis = emis.compute()
    for key, vals in criteria.items():
        for k, v in vals.items():
            ix = (emis[key] == k)
            emis.loc[ix, key] = v
    emis = dd.from_pandas(emis, chunksize=cfg.runtime.dask_bs)
    return emis


def preprocess_inventory(inventory):

    log.info("Preparing inventories: {}".format(inventory))
    log.debug("Loading inventory: {}".format(inventory))

    emis = prepare_inventory(
        cfg.inventories[inventory].paths.original,
        read_griddes(cfg.inventories[inventory].grid))

    log.debug("Filtering inventory")

    try:
        emis = filter_inventory(
            emis, criteria=cfg.inventories[inventory].filters)
    except AttributeError:
        log.info(
            "No filter criteria found for inventory: {}".format(inventory))

    try:
        emis = replace_in_inventory(
            emis, criteria=cfg.inventories[inventory].replacement_values)
    except AttributeError:
        log.info(
            "No replacement values found for inventory: {}".format(inventory))

    # add timezone information (needs to be done after name corrections)
    log.debug("Adding timezone information to emission sources")
    emis = add_timezone(emis, nproc=cfg.runtime.nproc)

    # read output model horizontal and vertical grids
    # TODO why metgrid?
    # TODO this is WRF-specific
    # TODO cache this information, as it takes 17s for domain 3
    log.debug("Read output grid definition")
    output_def_hor = read_output_grid(
        cfg.model.path_metgrid.format(domain=cfg.model.domain))
    output_def_vert = read_model_vertical_structure(
        cfg.model.path_wrfinput.format(domain=cfg.model.domain))

    # Set-up vertical distribution algorithm
    # TODO move vertical distribution to submodule
    log.debug("Initialize vertical re-gridder")
    if cfg.inventories[inventory].vertical_distribution == 'emep':
        vertdist = EMEPVerticalDistribution()
    else:
        raise ValueError()

    # Map sectors from inventory to vertical-distribution
    # TODO refactor to vertical distribution submodule
    SECTOR_MAPPING_INVENTORY_TO_VERTICAL = {
        1: 'SNAP1',
        2: 'SNAP2',
        5: 'SNAP5',
        6: 'SNAP6',
        8: 'SNAP8',
        9: 'SNAP9',
        10: 'SNAP10',
        34: 'SNAP3',  # TODO check if this makes sense
        71: 'SNAP7',
        72: 'SNAP7',
        73: 'SNAP7',
        74: 'SNAP7',
        75: 'SNAP7',
        81: 'SNAP8',
       }

    log.debug("Gridding inventory")
    emis = regrid(
        emis, read_griddes(cfg.inventories[inventory].grid),
        output_def_hor, output_def_vert,
        vertdist, SECTOR_MAPPING_INVENTORY_TO_VERTICAL)

#    if DO_DEBUG: pdb.set_trace()
#    if DO_DEBUG: pdb.set_trace()

    if cfg.inventories[inventory].paths.preprocessed:
        path_tmp = cfg.inventories[inventory].paths.preprocessed.format(
            domain=cfg.model.domain)
        log.debug("Saving pre-processed inventory data to {}".format(path_tmp))
        emis.to_parquet(path_tmp, engine='fastparquet')

    log.debug("Setting Index")
    emis = emis.set_index('sect', drop=False)

    return emis
