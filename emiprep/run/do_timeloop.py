from copy import copy
import logging
import pdb

import pandas as pd
import dask
import dask.dataframe as dd
import xarray as xr

from ..config import cfg
from ..sandbox.cycles import (
    prepare_emep_daily_factors, prepare_emep_hourly_factors,
    prepare_emep_monthly_factors, read_emep_country_map,
    add_emep_daily_factors, add_emep_hourly_factors, add_emep_monthly_factors)
from ..sandbox.models.wrf import create_wrfchemi
from ..regrid.units import convert_units
from ..sandbox.regrid import grid_emissions


log = logging.getLogger('emiprep')
DO_DEBUG = False


def create_output(emis):
    emis = xr.concat(emis, dim='time')

    if DO_DEBUG:
        log.debug("Saving debugging output to file {}".format('debug_grid.nc'))
        emis.to_netcdf('debug_grid.nc')

    PATH_OUTPUT = cfg.output.path.format(domain=int(cfg.model.domain),
                                         chem_opt=cfg.model.chem_opt)
    log.debug("Saving wrfchemi files to {}".format(PATH_OUTPUT))
    create_wrfchemi(
        emis, PATH_OUTPUT, cfg.time.start, cfg.model.domain,
        io_style=cfg.model.io_style, output_attrs=cfg.output.ncattrs,
        deflate=cfg.output.compress, nocolons=cfg.output.nocolons)


def del_emep_cols(emis):
    for col in ['country_emep', 'sect_emep', 'emep_iso2', 'emep_iso3',
                'species_emep']:
        try:
            del emis[col]
        except KeyError:
            pass
    return emis


# species split
def read_species_split(path):
    # see https://stackoverflow.com/a/13386025/152439 for stripping extra
    # whitespace

    def _strip(text):
        try:
            return text.strip()
        except AttributeError:
            return text

    df = pd.read_csv(
        path, sep=';', header=None, comment='#',
        names=('species', 'species_new', 'sect', 'country', 'weight_spec'),
        converters={key: _strip
                    for key in ('species', 'species_new', 'sect', 'country')})

    return df


def apply_split(emis, splitdata):
    # TODO why can't we do this in pre-processing?
    log.debug("Prepare sector-dependent species split table: expand sectors")
    all_sectors = emis['sect'].unique()
    split_new = []
    for _, sp in splitdata.iterrows():
        if sp['sect'] == '*':
            sect = all_sectors
        else:
            sect = [int(s_) for s_ in sp['sect'].split(',')]
        for s_ in sect:
            split_new.append(
                pd.Series(
                    [sp['species'], sp['species_new'], s_, sp['country'], sp['weight_spec']],
                    index=['species', 'species_new', 'sect', 'country', 'weight_spec']))
    splitdata = pd.DataFrame(split_new)
    splitdata = splitdata.sort_values(['species', 'sect'])

    log.info("Get list of all countries")
    all_countries = emis['country'].unique()

    log.debug("Prepare sector-dependent species split table: expand countries")
    split_new = []
    for _, sp in splitdata.iterrows():
        if sp['country'] == '*':
            countries = all_countries
        else:
            countries = [c_.strip() for c_ in sp['country'].split(',')]
        for c_ in countries:
            split_new.append(
                pd.Series(
                    [sp['species'], sp['species_new'], sp['sect'], c_, sp['weight_spec']],
                    index=['species', 'species_new', 'sect', 'country', 'weight_spec']))
    splitdata = pd.DataFrame(split_new)
    splitdata = splitdata.sort_values(['species', 'sect', 'country'])

    log.debug("Apply species split")
    del emis['species']
    e = emis.merge(splitdata, on=['species', 'sect', 'country'])
    return e


def apply_weights(emis):
    # aggregate weighting factors
    emis['weight_total'] = 1.0
    log.debug("Aggregate regridding weights")
    for key in ['weight_units', 'weight_regrid', 'weight_vert', 'weight_spec']:
        emis['weight_total'] *= emis[key]
    log.debug("Aggregate temporal cycle weights")
    for key in ['weight_daily', 'weight_weekly', 'weight_annual']:
        if key in emis.columns:
            emis['weight_total'] *= emis[key]
    log.debug("Done aggregating weights")

    # apply weighting factors
    log.debug("Apply weights")
    emis_vals = emis['emis_orig'] * emis['weight_total']
    emis['emis'] = emis_vals

    # don't carry on zero emissions
    log.debug("Remove zero emissions")
    emis = emis[emis['emis'] != 0.]
    return emis


def process_timestep(inv, em, t):
    log.info("Processing {}".format(t))

    # FIXME: this is still a dummy
    mysplit = read_species_split(cfg.inventories[inv].species_split.format(
        chem_opt=cfg.model.chem_opt))

    emep_country_map = read_emep_country_map(
        cfg.countries.path,
        extra_countries=cfg.countries.extra_countries,
        replacement_values=cfg.countries.replacement_values)

    # TODO this is a hack; make this more generic
    m_factors = prepare_emep_monthly_factors(
        cfg.inventories[inv].time_factors.monthly.path, emep_country_map, t,
        True, extra_countries=cfg.countries.extra_countries)
    d_factors = prepare_emep_daily_factors(
        cfg.inventories[inv].time_factors.daily.path, emep_country_map, t,
        True, extra_countries=cfg.countries.extra_countries)
    h_factors = prepare_emep_hourly_factors(
        cfg.inventories[inv].time_factors.hourly.path, t, True)

    if DO_DEBUG: pdb.set_trace()
    log.info("Processing inventory {} for timestep {}".format(inv, t))
    emis_tmp = em

    if DO_DEBUG: pdb.set_trace()

    log.debug("Add hourly factors for diurnal cycle")
    emis_tmp = add_emep_hourly_factors(emis_tmp, h_factors, t)
    emis_tmp = del_emep_cols(emis_tmp)

    if DO_DEBUG: pdb.set_trace()

    log.debug("Add daily factors for weekly cycle")
    emis_tmp = add_emep_daily_factors(emis_tmp, d_factors, t)
    # TODO properly handle the `del_emep_cols` issue
    emis_tmp = del_emep_cols(emis_tmp)

    if DO_DEBUG: pdb.set_trace()

    log.debug("Add monthly factors for seasonal cycle")
    emis_tmp = add_emep_monthly_factors(emis_tmp, m_factors, t)
    # emis_tmp = del_emep_cols(emis_tmp)

    if DO_DEBUG: pdb.set_trace()

    log.debug("Species split")
    emis_tmp = emis_tmp.set_index('species', drop=False)
    emis_tmp = apply_split(emis_tmp, mysplit)

    # convert units
    # TODO add gridcell_area units to df in regridding (above)
    if DO_DEBUG: pdb.set_trace()
    log.debug("Convert units")
    emis_tmp = convert_units(
        emis_tmp, cfg.model.species_definition.data, 't gridcell-1 yr-1')

    # calculate total emissions
    if DO_DEBUG: pdb.set_trace()

    # log.debug("Apply weights: split")
    # n_emis_per_part = 1000000
    # n_partitions = emis_tmp.shape[0] // n_emis_per_part + 1
    # emis_split = [emis_tmp.iloc[n_ * n_emis_per_part:(n_+1) * n_emis_per_part]
    #               for n_ in range(n_partitions)]

    # log.debug("Apply weights: calc")
    # emis_tmp = []
    # for (ii, em_) in enumerate(emis_split):
    #     print("{} / {}".format(ii + 1, n_partitions))
    #     emis_tmp.append(apply_weights(em_))

    # log.debug("Apply weights: concat")
    # emis_tmp = pd.concat(emis_tmp)

#    emis_tmp = dd.from_pandas(emis_tmp, chunksize=cfg.runtime.dask_bs)
    emis_tmp = apply_weights(emis_tmp)

    # single-threaded, to save memory
#    with dask.config.set(scheduler='single-threaded'):
#        emis_tmp = emis_tmp.compute()

#    if DO_DEBUG:
#        log.debug("Saving debug output to {}".format(PATH_DEBUG[key]))
#        emis_tmp.to_parquet(PATH_DEBUG[key])

    return emis_tmp


def run_timeloop(inventories):
    log.info("Starting time loop for emission distribution")
    # TODO put the Timestamp objects into the cfg dict
    t = copy(cfg.time.start)

    # initialize output arrays
    emis = []

    # TODO add utfoffset column once and not each timestep

    while t <= cfg.time.end:
        log.debug("Processing time step: {:%Y-%m-%dT%H:%M:%S}".format(t))
        emis_tmp = []
        for inv, em in inventories.items():
            log.debug("  Inventory: {}".format(inv))
            emis_tmp.append(process_timestep(inv, em, t))

        log.debug("Grid emission data")
        emis_df = pd.concat(emis_tmp)
        emis_grid = grid_emissions(
            emis_df, cfg.model.output_def_hor, t,
            cfg.model.species_definition.data)
        del emis_df

        emis.append(emis_grid)
        t += cfg.time.delta_t

    create_output(emis)
