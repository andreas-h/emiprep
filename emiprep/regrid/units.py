import logging


log = logging.getLogger('emiprep')


def _get_mass_conversion_factor(inventory_units, target_units, molweight):

    # mass conversion factor
    if 'mol' in target_units:
        m_fact = 1. / molweight
    elif 'ug' in target_units:
        m_fact = 1000000.
    elif 'g' in target_units:
        m_fact = 1.
    elif 'kg' in target_units:
        m_fact = 1. / 1000.
    elif 't' in target_units:
        m_fact = 1. / 1000000.
    else:
        raise ValueError(
            "Cannot convert mass from '{}' to '{}'".format(
                inventory_units, target_units))

    if 'mol' in inventory_units:
        m_fact *= molweight
    if 'g' in inventory_units:
        m_fact *= 1.
    elif 'kg' in inventory_units:
        m_fact *= 1000.
    elif 't' in inventory_units:
        m_fact *= 1000000.
    else:
        raise ValueError(
            "Cannot convert mass from '{}' to '{}'".format(
                inventory_units, target_units))

    return m_fact


def _get_area_conversion_factor(inventory_units, target_units, molweight):

    # area conversion factor
    # assume that gridcell_area is in m2
    if 'm-2' in target_units:
        a_fact = 1.
    elif 'km-2' in target_units:
        a_fact = 1000000.
    else:
        raise ValueError(
            "Cannot convert area from '{}' to '{}'".format(
                inventory_units, target_units))

    if 'm-2' in inventory_units:
        a_fact *= 1.
    elif 'km-2' in inventory_units:
        a_fact /= 1000000.
    # some inventories have emissions in 'per gridcell'
    # assume that gridcell area will be in m2
    elif 'gridcell-1' in inventory_units:
        a_fact *= 1.
    else:
        raise ValueError(
            "Cannot convert area from '{}' to '{}'".format(
                inventory_units, target_units))

    return a_fact


def _get_time_conversion_factor(inventory_units, target_units, molweight):

    # time conversion factor
    if 's-1' in target_units:
        t_fact = 1. / 3600.
    elif 'h-1' in target_units:
        t_fact = 1.
    elif 'd-1' in target_units:
        t_fact = 24.
    elif 'yr-1' in target_units:
        t_fact = 365.25 * 24.
    else:
        raise ValueError(
            "Cannot convert time from '{}' to '{}'".format(
                inventory_units, target_units))

    if 's-1' in inventory_units:
        t_fact *= 3600.
    elif 'h-1' in inventory_units:
        t_fact *= 1.
    elif 'd-1' in inventory_units:
        t_fact /= 24.
    elif 'yr-1' in inventory_units:
        t_fact /= 365.25 * 24.
    else:
        raise ValueError(
            "Cannot convert time from '{}' to '{}'".format(
                inventory_units, target_units))

    return t_fact


def convert_units(emis, species, inventory_units):

    # TODO the gridcell area should be here as well
    def _conv(row, inventory):
        target_units = [s.strip() for s in row.units.split()]
        inventory_units = inventory.split()
        m_fact = _get_mass_conversion_factor(
            inventory_units, target_units, row.molweight)
        a_fact = _get_area_conversion_factor(
            inventory_units, target_units, row.molweight)
        t_fact = _get_time_conversion_factor(
            inventory_units, target_units, row.molweight)
        return a_fact * m_fact * t_fact

    fact = species['species'].to_frame()
    log.debug("Calculate unit conversion factors")
    fact['weight_units'] = species.apply(
        _conv, args=(inventory_units, ), axis=1)
    log.debug("Apply unit conversion factors")
    em = emis.merge(
        fact.rename(columns={'species': 'species_new'}), on='species_new')
    log.debug("Done with unit conversion")

    return em
