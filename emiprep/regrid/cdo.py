"""emiprep.regrid.cdo
==================

This module contains methods to regrid to WRF grids using CDO_.

.. currentmodule:: emiprep.regrid.cdo

.. autosummary::
   :toctree: api/

   metgrid_to_cdo_griddes

   _metgrid_to_cdo_grid_info_extraction
   _mkblock

.. _CDO: https://code.mpimet.mpg.de/projects/cdo

"""

from __future__ import (
    absolute_import, division, print_function, unicode_literals)

from builtins import int, open, range
import logging
import os
import shutil
import tempfile

from io import BytesIO, StringIO

import dask.array as da
from numba import njit, prange
import numpy as np
import pandas as pd
from shapely.geometry import Point, LineString
from sklearn.neighbors import BallTree
import xarray as xr

from cdo import Cdo

from ..util import modified_environ


CDO_BINARY = shutil.which('cdo')

log = logging.getLogger('emiprep')


def _metgrid_to_cdo_grid_info_extraction(fn_metgrid):
    """Extract grid information from WPS metgrid file.

    Parameters
    ----------
    fn_metgrid : str
        The full path to a WPS metgrid file from which the grid information
        shall be extracted

    Returns
    -------
    gridinfo : xarray.Dataset
        The grid information of the metgrid file

    """
    METGRID_SCRIP_TRANSFORM = {
        'XLAT_M': 'grid_center_lat', 'XLONG_M': 'grid_center_lon',
        'south_north': 'grid_ysize', 'west_east': 'grid_xsize'}

    log.debug("  Open met_em file")
    ds = xr.open_dataset(fn_metgrid)
    ds = ds.rename(METGRID_SCRIP_TRANSFORM)

    lat_center = ds['grid_center_lat'][0]
    lon_center = ds['grid_center_lon'][0]
    if lat_center.shape != lon_center.shape:
        raise ValueError("Shapes of metgrid XLAT_M and XLONG_M don't match")

    # create corner arrays
    lat_corner_orig = ds['XLAT_C'][0].values
    lon_corner_orig = ds['XLONG_C'][0].values
    if lat_corner_orig.shape != lon_corner_orig.shape:
        raise ValueError("Shapes of metgrid XLAT_C and XLONG_C don't match")

    log.debug("  Create empty coordinate arrays")

    @njit(parallel=True)
    def _create_coordinate_arrays(lat_corner_orig, lon_corner_orig):
        nx, ny = lat_corner_orig.shape[0] - 1, lat_corner_orig.shape[1] - 1
        lat_corner = np.zeros((nx, ny, 4), dtype=np.float32)
        lon_corner = np.zeros((nx, ny, 4), dtype=np.float32)

        for ii in prange(nx):
            for jj in prange(ny):
                lat_corner[ii, jj, 0] = lat_corner_orig[ii, jj]
                lat_corner[ii, jj, 1] = lat_corner_orig[ii, jj + 1]
                lat_corner[ii, jj, 2] = lat_corner_orig[ii + 1, jj + 1]
                lat_corner[ii, jj, 3] = lat_corner_orig[ii + 1, jj]
                lon_corner[ii, jj, 0] = lon_corner_orig[ii, jj]
                lon_corner[ii, jj, 1] = lon_corner_orig[ii, jj + 1]
                lon_corner[ii, jj, 2] = lon_corner_orig[ii + 1, jj + 1]
                lon_corner[ii, jj, 3] = lon_corner_orig[ii + 1, jj]

        return lat_corner, lon_corner

    log.debug("  Fill coordinate arrays")
    lat_corner, lon_corner = _create_coordinate_arrays(
        lat_corner_orig, lon_corner_orig)
    lat_corner = xr.DataArray(
        lat_corner,
        dims=['grid_ysize', 'grid_xsize', 'grid_corners'],
        name='grid_corner_lat')
    lon_corner = xr.DataArray(
        lon_corner,
        dims=['grid_ysize', 'grid_xsize', 'grid_corners'],
        name='grid_corner_lon')

    # need to include data variable in nc file, else CDO will complain
    dummy = xr.DataArray(
        np.zeros((lat_center.shape[0], lat_center.shape[1]), dtype=np.float32),
        dims=['grid_ysize', 'grid_xsize'], name='dummydata')
    dummy.attrs['coordinates'] = 'grid_center_lon grid_center_lat'

    # fix metadata
    log.debug("  Fix metadata")
    for arr in lat_center, lon_center, lat_corner, lon_corner:
        arr.attrs['units'] = 'degrees'
    lat_center.attrs['standard_name'] = 'latitude'
    lon_center.attrs['standard_name'] = 'longitude'
    lat_center.attrs['bounds'] = 'grid_corner_lat'
    lon_center.attrs['bounds'] = 'grid_corner_lon'

    log.debug("  Prepare output")
    n_grid_size = lat_center.shape[0] * lat_center.shape[1]
    grid_imask = xr.DataArray(
        np.ones(n_grid_size, dtype=int),
        {'grid_size': np.arange(1, n_grid_size + 1)}, ['grid_size'],
        name='grid_imask')
    grid_imask.attrs['coordinates'] = 'grid_center_lon grid_center_lat'
    grid_imask.attrs['units'] = 'unitless'

    ds_new = xr.Dataset({arr.name: arr for arr in [lat_center, lon_center,
                                                   lat_corner, lon_corner,
                                                   grid_imask,
                                                   dummy]})

    return ds_new


def _mkblock(arr, width=10, sep=None):
    """Create a block of string-formatted floats to be saved in CDO grid file

    Parameters
    ----------
    arr : numpy.ndarray
        The array to format

    width : int
        How many numbers to put on one line

    sep : str
        The line separator.  This will usually inlcude any leading spaces
        needed in subsequent lines after the first.  By default, 12 spaces will
        be inserted after each linebreak, to generate nicely formatted griddes
        files.

    Returns
    -------
    griddes : str
        The string containing *arr*.

    """
    if sep is None:
        sep = '\n            '

    # how many rows do we need to generate?
    nrows = arr.size // width
    if arr.size % width:
        nrows += 1

    arr_slices = [arr.ravel()[i * width:(i + 1) * width] for i in range(nrows)]
    with BytesIO() as fd:
        for sl in arr_slices:
            # apparently, np.savetxt cannot handle unicode fmt as py2
            np.savetxt(fd, np.atleast_2d(sl), fmt=b'%11.7f', newline=sep)
        griddes = fd.getvalue().decode()

    # remove empty lines
    griddes = '\n'.join(
        [line for line in griddes.split('\n') if line.strip() != ''])

    return griddes


def metgrid_to_cdo_griddes(fn_metgrid, fn_griddes=None):
    # TODO refactor this to go to the WRF module
    """Create a CDO grid description file from a WPS metgrid file.

    Parameters
    ----------
    fn_metgrid : str
        The full path to a WPS metgrid file from which the grid information
        shall be extracted
    fn_griddes : str, optional
        If given, the grid description will be written to that location.  Any
        existing file with the same name will be overwritten.

    Returns
    -------
    griddes : str
        The CDO grid description

    """
    griddes = _metgrid_to_cdo_grid_info_extraction(fn_metgrid)

    with StringIO() as fd:
        fd.write('gridtype  = curvilinear\n')
        fd.write('gridsize  = {}\n'.format(griddes.dims['grid_size']))
        fd.write('xsize     = {}\n'.format(griddes.dims['grid_xsize']))
        fd.write('ysize     = {}\n'.format(griddes.dims['grid_ysize']))

        fd.write('xvals     = {}\n'.format(
            _mkblock(griddes['grid_center_lon'].values)))
        fd.write('xbounds   = {}\n'.format(
            _mkblock(griddes['grid_corner_lon'].values, width=8)))

        fd.write('yvals     = {}\n'.format(
            _mkblock(griddes['grid_center_lat'].values)))
        fd.write('ybounds   = {}\n'.format(
            _mkblock(griddes['grid_corner_lat'].values, width=8)))

        retval = fd.getvalue()

    if fn_griddes:
        with open(fn_griddes, 'w') as fd:
            fd.write(retval)
    return retval


def _mkgriddes(griddict):
    """Convert a dictionary to CDO grid description string"""
    with StringIO() as fd:
        GRIDDES_KEYS = [
            'gridtype', 'gridsize', 'xsize', 'ysize', 'xfirst', 'yfirst',
            'xinc', 'yinc', 'xvals', 'xbounds', 'yvals', 'ybounds']
        for key in GRIDDES_KEYS:
            try:
                gridstr = griddict[key]
                try:
                    gridstr = _mkblock(gridstr)
                except:  # TODO specify exception, which would be raised?
                    pass
                fd.write('{} = {}\n'.format(key, gridstr))
            except KeyError:
                pass

        retval = fd.getvalue()
    return retval


def calc_gridcell_area(griddes, earth_radius=6371000):
    """Calculate the gridcell area using CDO

    Parameters
    ----------
    griddes : str
        The CDO grid description (what would be written to the grid
        description file for use by CDO).

    earth_radius : int, optional
        The Earth radius to be used for calculating the gridcell area,
        in meters.

    Returns
    -------
    gridcell_area : np.ndarray
        Array containing the grid cell area, shape (lon, lat)

    """
    # write griddes file to temporarly location
    # TODO re-factor to use emiprep.util
    _, fn_griddes = tempfile.mkstemp()
    # TODO re-factor to have functions to translate dict to str and vice-versa
    if isinstance(griddes, dict):
        griddes = _mkgriddes(griddes)
    with open(fn_griddes, 'w') as fd:
        fd.write(griddes)

    with modified_environ(CDO=CDO_BINARY):
        cdo = Cdo()
        area = cdo.gridarea(
            options='-f nc', returnArray='cell_area',
            env={'PLANET_RADIUS': str(earth_radius)},
            input='-const,1,{}'.format(fn_griddes))

    del cdo
    os.remove(fn_griddes)

    # reaturn lon/lat instead of lat/lon
    area = area.T

    return area


def prepare_remap_weights(griddes_src, griddes_dst, norm='destarea',
                          minarea=0.0, n_cpu=1, debug=False):
    """Prepare remapping weights

    Due to the way CDO's *genycon* operator works, we need to run it
    twice: For those grid cells on the output grid edge, we need to
    run it additionally, interchanging source and target grids.
    Otherwise, we have too high emissions on the output grid edges,
    since CDO assigns 100% of an inventory grid cell to the
    intersecting output grid cells.

    Parameters
    ----------
    griddes_src : dict

    griddes_dst : dict

    norm : str, optional
        This variable is used to choose the normalization of the
        conservative interpolation.  By default CDO_REMAP_NORM is set
        to 'destarea'.  'destarea' uses the total target cell area to
        normalize each target cell field value. Local flux
        conservation is ensured, but unreasonable flux values may
        result.  The option 'fracarea' on the other hand uses the sum
        of the non-masked source cell intersected areas to normalize
        each target cell field value.  This results in a reasonable
        flux value but the flux is not locally conserved.

    minarea : float, optional
        This variable is used to set the minimum destination area fraction.
        The default of this variable is 0.0.

    n_cpu : int, optional
        The number of CPU cores to use for the CDO remap operation

    Returns
    -------
    weights : pandas.DataFrame
        DataFrame with the three columns ``src_address``,
        ``dst_address``, and ``weight``

    """
    # NOTE contrary to my manual sensitivity tests, it seems as if the
    #      special treatment for the edge grid cells is not needed.
    #      For now, I'll leave the code here, but it seems as if the
    #      only thing we need to do in this function is the switch
    #      between srcgrid and dstgrid

    # weights_edge = calc_remap_weights(
    #     griddes_src, griddes_dst, norm, minarea, n_cpu, debug)
    weights_center = calc_remap_weights(
        griddes_dst, griddes_src, norm, minarea, n_cpu, debug)
    weights_center = weights_center.rename(
        columns=dict(ix_srcgrid='ix_dstgrid', ix_dstgrid='ix_srcgrid'))
    return weights_center

    # # calculate indices of the edge grid cells
    # nx, ny = griddes_dst['xsize'], griddes_dst['ysize']
    # ix_edge = pd.Series(index=weights_edge.index, dtype=bool)
    # ix_edge[:] = False
    # ix_edge.iloc[:nx] = True
    # ix_edge.iloc[nx:] = True
    # ix_edge.iloc[ix_edge.index % nx == 0] = True
    # ix_edge.iloc[ix_edge.index % nx == nx - 1] = True

    # def _is_edge(x):
    #     return (x < nx or x > nx * (ny - 1) or x % nx == 1 or x % nx == 0)

    # w_edge = weights_edge[weights_edge['ix_dstgrid'].apply(_is_edge)]
    # w_center = weights_center[~weights_center['ix_dstgrid'].apply(_is_edge)]

    # weights = pd.concat((w_edge, w_center))
    # weights['is_edge'] = weights['ix_dstgrid'].apply(_is_edge)
    # return weights


def calc_remap_weights(griddes_src, griddes_dst, norm='destarea',
                       minarea=0.0, n_cpu=1, debug=False):
    """Calculate remapping weights using CDO

    This function calculates area remapping weights using the `cdo genycon`
    operator.

    Parameters
    ----------
    griddes_src : dict

    griddes_dst : dict

    norm : str, optional
        This variable is used to choose the normalization of the
        conservative interpolation.  By default CDO_REMAP_NORM is set
        to 'destarea'.  'destarea' uses the total target cell area to
        normalize each target cell field value. Local flux
        conservation is ensured, but unreasonable flux values may
        result.  The option 'fracarea' on the other hand uses the sum
        of the non-masked source cell intersected areas to normalize
        each target cell field value.  This results in a reasonable
        flux value but the flux is not locally conserved.

    minarea : float, optional
        This variable is used to set the minimum destination area fraction.
        The default of this variable is 0.0.

    n_cpu : int, optional
        The number of CPU cores to use for the CDO remap operation

    Returns
    -------
    weights : pandas.DataFrame
        DataFrame with the three columns ``src_address``,
        ``dst_address``, and ``weight``

    References
    ----------
    https://code.zmaw.de/projects/cdo/embedded/index.html#x1-5970002.12.5

    """
    if not isinstance(griddes_src, dict):
        raise ValueError("`griddes_src` must be of type `dict`.")
    if not isinstance(griddes_dst, dict):
        raise ValueError("`griddes_dst` must be of type `dict`.")
    # TODO re-factor tmpfile generation to use emiprep.util
    _, fn_src = tempfile.mkstemp()
    _, fn_dst = tempfile.mkstemp()
    with open(fn_src, 'w') as fd:
        fd.write(_mkgriddes(griddes_src))
    with open(fn_dst, 'w') as fd:
        fd.write(_mkgriddes(griddes_dst))

    opts = '-f nc'

    if isinstance(n_cpu, int):
        opts += ' -P {n_cpu}'.format(n_cpu=str(n_cpu))
    else:
        raise ValueError(
            "Invalid type of 'n_cpu': {}".format(type(n_cpu)))

    env = {}

    if norm not in ['destarea', 'fracarea']:
        raise ValueError("Invalid choice of 'norm': '{}'".format(norm))
    else:
        env['CDO_REMAP_NORM'] = norm

    if not isinstance(minarea, float):
        raise ValueError(
            "Invalid type of 'minarea': {}".format(type(minarea)))
    elif minarea < 0.:
        raise ValueError("'minarea' cannot be negative")
    else:
        env['REMAP_AREA_MIN'] = str(minarea)

    log.debug("  Run cdo.genycon")
    with modified_environ(CDO=CDO_BINARY):
        cdo = Cdo()
        fd = cdo.genycon(
            os.path.abspath(fn_dst), options=opts, env=env, returnCdf=True,
            input='-const,1,{}'.format(os.path.abspath(fn_src)))
    log.debug("  Done with cdo.genycon")

    if debug:
        df_ = {k: fd.variables[k][:] for k in fd.variables}
    else:
        addr_src = fd.variables['src_address'][:]
        addr_dst = fd.variables['dst_address'][:]
        weights = fd.variables['remap_matrix'][:]
        df_ = pd.DataFrame(
            {'ix_srcgrid': addr_src, 'ix_dstgrid': addr_dst,
             'weight': weights.squeeze()})

    for fn_ in fn_src, fn_dst, fd.filepath():
        try:
            os.remove(fn_)
        except IOError:
            pass
    del cdo

    return df_


def get_srcgrid_index(ix_lon, ix_lat, griddes):
    """Calculate index of emission source in remap weight index

    For each emission source (defined by ``ix_lon`` and ``ix_lat``),
    calculate the ``src_address`` (or, ``ix_srcgrid`` in emiprep's
    emission DataFrame).
    """
    # TODO re-factor to have a more generic name, i.e., get_grid_index_1D
    nx = griddes['xsize']
    # CDO's `src_address` has longitude as inner loop, i.e., is (lat, lon)
    ix = np.asarray(ix_lon) + np.asarray(ix_lat) * nx
    # CDO's `src_address` (ix_srcgrid in emiprep's `emis` DF) starts with 1
    return 1 + ix


def coords_to_griddes(lons, lats, radians=False):
    """Create CDO grid description for regular lon/lat grid"""
    if radians:
        raise NotImplementedError()
    xdef = _coord_to_griddes(lons)
    ydef = _coord_to_griddes(lats)
    griddes = """gridtype = lonlat
xsize    =  {nx}
ysize    =  {ny}
xfirst   =  {x0:.7f}
xinc     =  {dx:.7f}
yfirst   =  {y0:.7f}
yinc     =  {dy:.7f}""".format(nx=xdef[0], ny=ydef[0], x0=xdef[1],
                               y0=ydef[1], dx=xdef[2], dy=ydef[2])
    return griddes


def _coord_to_griddes(arr):
    if len(arr.shape) != 1:
        raise ValueError()
    arr_ = np.sort(arr)
    diff_ = np.diff(arr_)
    diff__ = np.diff(diff_)
    if not np.allclose(diff__, 0.):
        raise ValueError()
    n = arr.size
    x0 = arr_[0]
    d = diff_[0]
    return n, x0, d


def _get_xy_index_lonlat(lons, lats, griddes):
    """Give grid cell indices for a list of lon/lat coordinates in a CDO grid
    """
    def _get_ix(coords, griddes, x_or_y):
        try:
            coords = coords.compute()
        except AttributeError:
            pass
        cmin = griddes['{}first'.format(x_or_y)]
        cmax = cmin + (griddes['{}size'.format(x_or_y)] *
                       griddes['{}inc'.format(x_or_y)])
        grid = np.linspace(cmin, cmax, griddes['{}size'.format(x_or_y)] + 1)

        ix = np.digitize(coords, grid, )
        ix -= 1  # shift left by one, as we want the cell index
        # coords exactly on the right edge will fall outside, so we
        # need to subtract those by an additional 1
        ix[np.isclose(coords, cmax, rtol=0.)] -= 1

        if (ix < 0).any() or (ix >= griddes['{}size'.format(x_or_y)]).any():
            raise ValueError("Out-of-bounds")

        return ix

    def _get_outofbounds(coords, griddes, x_or_y):
        if not isinstance(coords, da.Array):
            raise ValueError()
        bmin = (griddes['{}first'.format(x_or_y)] -
                griddes['{}inc'.format(x_or_y)] / 2)
        bmax = bmin + (griddes['{}size'.format(x_or_y)] *
                       griddes['{}inc'.format(x_or_y)])
        ix = (coords < bmin) | (coords > bmax)
        return ix

    log.debug("Getting x/y indices for point sources (lat/lon grid)")
    ix_lon = _get_ix(lons, griddes, 'x')
    ix_lat = _get_ix(lats, griddes, 'y')

    return ix_lon, ix_lat


def _get_xy_index_curvilinear(lons, lats, griddes):
    log.debug("Getting x/y indices for point sources (curvilinear grid)")
    try:
        lons, lats = lons.compute(), lats.compute()
    except AttributeError:
        pass
    gridlons, gridlats = griddes['xvals'], griddes['yvals']

    log.debug("  Prepare output index arrays")
    ix_lon = np.ones_like(lons, dtype=int) * -1
    ix_lat = np.ones_like(lats, dtype=int) * -1

    log.debug("  Calculate convex hull of target grid")
    boundary_points = [Point((x, y)) for x, y in zip(
        griddes['xbounds'], griddes['ybounds'])]
    boundary_shape = LineString(boundary_points).convex_hull

    log.debug("  Check for point sources in proximity of target grid")
    # index of those sources inside the rectangular bounding box of
    # the model domain
    minlon, maxlon = griddes['xbounds'].min(), griddes['xbounds'].max()
    minlat, maxlat = griddes['ybounds'].min(), griddes['ybounds'].max()
    ix_valid = ((lons >= minlon) & (lons <= maxlon) &
                (lats >= minlat) & (lats <= maxlat))

    log.debug("  Check for point sources within convex hull of target grid")
    # for those sources inside the bounding box, do point-in-polygon
    # lookup if source lies within convex hull of model domain
    ix_outofbounds = np.asarray([
        not boundary_shape.contains(Point((x, y))) for x, y in zip(
            lons[ix_valid].values, lats[ix_valid].values)])

    log.debug("  Construct index")
    # set those in ix_valid to False which are not inside the convex hull
    ix_v = ix_valid[ix_valid]
    ix_v[ix_outofbounds] = False
    ix_valid[ix_valid] = ix_v.values  # necessary, otherwise dtype=O ?!?
    del ix_v

    log.debug("  Preparing coordinate arrays")
    # coordinates of valid (i.e., inside convex hull) sources
    ptlons = lons[ix_valid]
    ptlats = lats[ix_valid]
    # prepare coordinate arrays for nearest-neighbor lookup
    emi = np.hstack((ptlats[:, np.newaxis], ptlons[:, np.newaxis]))
    grd = np.hstack((gridlats[:, np.newaxis], gridlons[:, np.newaxis]))
    emi = np.deg2rad(emi)
    grd = np.deg2rad(grd)

    log.debug("  Nearest neighbor lookup")
    # using sklearn, see https://twitter.com/jakevdp/status/854077564918276096
    bt = BallTree(grd, metric='haversine')
    _, ix = bt.query(emi, k=1)  # we don't actually need the distance

    log.debug("  Convert indices (1-D to 2-D)")
    ix_lat[ix_valid], ix_lon[ix_valid] = np.unravel_index(
        ix.ravel(), (griddes['ysize'], griddes['xsize']))

    return ix_lon, ix_lat


def get_xy_index(lons, lats, griddes):
    """Get grid cell indices"""
    if isinstance(griddes, str):
        with StringIO(griddes) as fd:
            griddes = read_griddes(fd)
    if griddes['gridtype'] == 'lonlat':
        # TODO take care of rotated pole grids?
        return _get_xy_index_lonlat(lons, lats, griddes)
    elif griddes['gridtype'] == 'curvilinear':
        return _get_xy_index_curvilinear(lons, lats, griddes)
    else:
        raise NotImplementedError


def read_griddes(griddes):
    """Read coordinate information from a CDO grid description file"""
    if isinstance(griddes, str):
        try:
            with open(griddes, 'r') as fd:
                lines = fd.readlines()
        except IOError:
            lines = griddes.split('\n')
    else:  # griddes is no string, so should be file-like
        lines = griddes.readlines()

    splitlines = [l.split('=') for l in lines]
    lenghts = [len(l) for l in splitlines]
    indices = [i for i, k in enumerate(splitlines)
               if lenghts[i] == 2] + [len(lines)]
    keys = [k[0].strip() for i, k in enumerate(splitlines) if lenghts[i] == 2]
    datalines = [l[1] if len(l) == 2 else l[0] for l in splitlines]
    data = {}
    for j, key in enumerate(keys):
        key = keys[j]
        if key in ['gridtype']:
            data[key] = datalines[indices[j]].strip()
        elif key in ['gridsize', 'xsize', 'ysize']:
            data[key] = int(datalines[indices[j]].strip())
        elif key in ['xfirst', 'yfirst', 'xinc', 'yinc']:
            data[key] = float(datalines[indices[j]].strip())
        else:
            values = []
            for i in range(indices[j], indices[j + 1]):
                values.append(np.fromstring(datalines[i].strip(), sep=' '))
            data[key] = np.concatenate(values)

    return data
