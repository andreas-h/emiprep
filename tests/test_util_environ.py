from __future__ import (
    absolute_import, division, print_function, unicode_literals)

import os

from .context import emiprep
from emiprep.util import modified_environ


def test_modified_environ():
    # from https://stackoverflow.com/a/34333710/152439

    home_before = os.environ.get('HOME')
    path_before = os.environ.get("LD_LIBRARY_PATH")

    with modified_environ('HOME', LD_LIBRARY_PATH='/my/path/to/lib'):
        home_inside = os.environ.get('HOME')
        path_inside = os.environ.get("LD_LIBRARY_PATH")

    assert home_inside is None
    assert path_inside == '/my/path/to/lib'

    home_after = os.environ.get('HOME')
    path_after = os.environ.get("LD_LIBRARY_PATH")

    assert home_before == home_after
    assert path_before == path_after
