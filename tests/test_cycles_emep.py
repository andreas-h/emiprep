import os.path

from .context import emiprep
from emiprep.cycles.emep import (
    read_countries, read_daily_factors, read_hourly_factors,
    read_monthly_factors)


def _mk_path(fn):
    return os.path.join(os.path.dirname(__file__), 'testdata', 'emep', fn)


FN_HOURLY = _mk_path('HOURLY-FACS')
FN_DAILY = _mk_path('DailyFac.co')
FN_MONTHLY = _mk_path('MonthlyFac.co')
FN_COUNTRIES = _mk_path('country_numbers.txt')


def test_read_countries():
    read_countries(FN_COUNTRIES)


def test_read_daily_factors():
    read_daily_factors(FN_DAILY, FN_COUNTRIES)


def test_read_hourly_factors():
    read_hourly_factors(FN_HOURLY)


def test_read_monthly_factors():
    read_monthly_factors(FN_MONTHLY, FN_COUNTRIES)
