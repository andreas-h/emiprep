.. Project Status
.. image:: https://img.shields.io/pypi/status/emiprep.svg
   :target: https://pypi.python.org/pypi/emiprep/
.. PyPI package
.. image:: https://img.shields.io/pypi/v/emiprep.svg
   :target: https://pypi.python.org/pypi/emiprep/
.. Anaconda package
.. image:: https://img.shields.io/conda/v/andreas-h/emiprep.svg
   :target: https://anaconda.org/andreas-h/emiprep
.. Python Versions
.. image:: https://img.shields.io/pypi/pyversions/emiprep.svg
   :target: https://pypi.python.org/pypi/emiprep/
.. License - license badge must use gh repo, as shields.io doesn't support gl
.. image:: https://img.shields.io/github/license/andreas-h/emiprep.svg
   :target: https://gitlab.com/andreas-h/emiprep/blob/develop/LICENSE
   :alt: License: AGPLv3
.. Build Status (develop branch)
.. image:: https://gitlab.com/andreas-h/emiprep/badges/develop/pipeline.svg
   :target: https://gitlab.com/andreas-h/emiprep/commits/develop
   :alt: Build Status
.. image:: https://ci.appveyor.com/api/projects/status/ww8ualuvkqy2ujfp?svg=true
   :target: https://ci.appveyor.com/project/andreas-h/emiprep
   :alt: Build Status (Windows)
.. Test coverage
.. image:: https://api.codeclimate.com/v1/badges/a75f12e3b6f10b7a36d0/test_coverage
   :target: https://codeclimate.com/repos/59b995cde1f80102ad00039e/test_coverage
   :alt: Test Coverage (Code Climate)
.. image:: https://codecov.io/gl/andreas-h/emiprep/branch/develop/graph/badge.svg
   :target: https://codecov.io/gl/andreas-h/emiprep
   :alt: Test Coverage (CodeCov)
.. Static Analysis
.. image:: https://api.codeclimate.com/v1/badges/a75f12e3b6f10b7a36d0/maintainability
   :target: https://codeclimate.com/repos/59b995cde1f80102ad00039e/maintainability
   :alt: Maintainability (Code Climate)
.. image:: https://api.codacy.com/project/badge/Grade/5ab78b2dd4d8436ebda2632c14e86739
   :target: https://www.codacy.com/app/andreas-h/emiprep?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=andreas-h/emiprep&amp;utm_campaign=Badge_Grade
   :alt: Codacy Report
.. image:: https://codebeat.co/badges/7d7ba498-3ab9-4c61-8415-8ef38a51d749
   :target: https://codebeat.co/projects/gitlab-com-andreas-h-emiprep-develop
   :alt: codebeat Report


=======
emiprep
=======

-------------------------------------------------------
Emission pre-processor for atmospheric chemistry models
-------------------------------------------------------

This is yet another emission pre-processor for use with atmospheric chemistry
models.  It is being developed for `WRF-Chem
<https://ruc.noaa.gov/wrf/wrf-chem/>`__, but there is no reason why it shouldn't
in principle also work for other models.


Contact
=======

In case of problems, please `open an issue
<https://gitlab.com/andreas-h/emiprep/issues/new>`__ or `send an e-mail
<incoming+andreas-h/emiprep@gitlab.com>`__ (which will create an issue, i.e.,
will be public).
